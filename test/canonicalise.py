#!/usr/bin/env python3
#
# Test the canonicalisation routines

from arpa2 import reservoir

testdom = '%s.canon' % reservoir.random_uuid ().split ('-') [0]
testusr = 'charlie'


def checkwrap (tup):
	(c14n,clx1,res1) = tup
	print ('c14n:', c14n)
	(clx2,res2) = reservoir.uri_canonical_open (c14n)
	if str (clx1) != str (clx2):
		print ('### ERROR: clx1=%s\n           clx2=%s' % (clx1,clx2))
	if str (res1) != str (res2):
		print ('### ERROR: res1=%s\n           res2=%s' % (res1,res2))
	if res1 is None:
		clx1_c14n = clx1.get_canonical_uri ()
		if clx1_c14n != c14n:
			print ('### ERROR: clx1=%s\n           c14n=%s' % (clx1_c14n,c14n))
	else:
		res1_c14n = res1.get_canonical_uri ()
		if res1_c14n != c14n:
			print ('### ERROR: res1=%s\n           c14n=%s' % (res1_c14n,c14n))
	return (c14n,clx1,res1)


domidx = reservoir.add_domain (testdom)
usridx = reservoir.add_domain_user (testdom, testusr)

#TODO# reservoir.add_apphint (domidx, 'music', acl='%w @.')
#TODO# reservoir.add_apphint (usridx, 'music', acl='%w @.')
reservoir.add_collection (domidx, 'music')
reservoir.add_collection (usridx, 'music')

domidx = reservoir.get_domain (testdom)
usridx = reservoir.get_domain_user (testdom, testusr)

domidx.set_apphint ('music')
usridx.set_apphint ('music')

domidx.use_apphint ()
usridx.use_apphint ()

#USELESS# print ('The domain is at %s' % domidx.get_colluuid ())
#USELESS# print ('The user   is at %s' % usridx.get_colluuid ())
#USELESS# print ()

(c14n,clx,res) = checkwrap (reservoir.uri_canonical (domain=testdom))

print ('Expected: //%s/%s/' % (testdom,clx.get_colluuid ()))
print ('Gotten:   %s' % c14n)
print ()

(c14n,clx,res) = checkwrap (reservoir.uri_canonical (domain=testdom, user=testusr))

print ('Expected: //%s/%s/' % (testdom,clx.get_colluuid ()))
print ('Gotten:   %s' % c14n)
print ()

print ('New music app at %s' % domidx.get_colluuid ())
print ()

(c14n,clx,res) = checkwrap (reservoir.uri_canonical (domain=testdom, user=testusr, apphint='music'))

print ('Expected: //%s/%s/' % (testdom,clx.get_colluuid ()))
print ('Gotten:   %s' % c14n)
print ()

reservoir.add_collection (clx, 'cymbal')
print ('Cymbals play  at %s' % domidx.get_colluuid ())
reservoir.add_collection (clx, 'single-handedly')
print ('Single-handed at %s' % domidx.get_colluuid ())
print ()

(c14n,clx,res) = checkwrap (reservoir.uri_canonical (domain=testdom, user=testusr, apphint='music', path=['cymbal', 'single-handedly']))

print ('Expected: //%s/%s/' % (testdom,clx.get_colluuid ()))
print ('Gotten:   %s' % c14n)
print ()

#NOTINDOMAIN# (c14n,clx,res) = reservoir.uri_canonical (domain=testdom, apphint='music', path=['cymbal', 'single-handedly'])
#NOTINDOMAIN# 
#NOTINDOMAIN# print ('Expected: //%s/%s/' % (testdom,clx.get_colluuid ()))
#NOTINDOMAIN# print ('Gotten:   %s' % c14n)
#NOTINDOMAIN# print ()


