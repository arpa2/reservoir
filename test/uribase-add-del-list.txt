Initially empty [0 entries]
START/EMPTY [enduser] [0 entries]
START/EMPTY [backend] [0 entries]
START/EMPTY [listall] [0 entries]

ftp+ [enduser] [1 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
ftp+ [backend] [0 entries]
ftp+ [listall] [1 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')

ftp+, raw+ [enduser] [2 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
ftp+, raw+ [backend] [0 entries]
ftp+, raw+ [listall] [2 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')

ftp+, raw+, dom- [enduser] [2 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
ftp+, raw+, dom- [backend] [1 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)
ftp+, raw+, dom- [listall] [3 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
 - ('https:///?apphint=music&domain=orvelte.nep', None)

ftp+, raw+, dom-, msg- [enduser] [2 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
ftp+, raw+, dom-, msg- [backend] [2 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)
 - ('amqps://amqp:1234/reservoir?apphint=inbox', None)
ftp+, raw+, dom-, msg- [listall] [4 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
 - ('https:///?apphint=music&domain=orvelte.nep', None)
 - ('amqps://amqp:1234/reservoir?apphint=inbox', None)

ftp+, dom-, msg- [enduser] [1 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
ftp+, dom-, msg- [backend] [2 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)
 - ('amqps://amqp:1234/reservoir?apphint=inbox', None)
ftp+, dom-, msg- [listall] [3 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https:///?apphint=music&domain=orvelte.nep', None)
 - ('amqps://amqp:1234/reservoir?apphint=inbox', None)

ftp+, dom- [enduser] [1 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
ftp+, dom- [backend] [1 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)
ftp+, dom- [listall] [2 entries]
 - ('ftp://ftp', 'Retro File Transfer Protocol')
 - ('https:///?apphint=music&domain=orvelte.nep', None)

dom- [enduser] [0 entries]
dom- [backend] [1 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)
dom- [listall] [1 entries]
 - ('https:///?apphint=music&domain=orvelte.nep', None)

FINISH/EMPTY [enduser] [0 entries]
FINISH/EMPTY [backend] [0 entries]
FINISH/EMPTY [listall] [0 entries]

