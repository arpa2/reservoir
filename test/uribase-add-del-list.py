#!/usr/bin/env python3
#
# list, add, del base URIs in a domain Home Index.


from arpa2 import reservoir

domain = '%s.baseuri' % reservoir.random_uuid ().split ('-') [0]


domidx = reservoir.add_domain (domain)

def nicelist (hd, xs):
	print ('%s [%d entries]' % (hd,len(xs)))
	for x in xs:
		print (' -', x)

def triplelist (hd):
	global domidx
	lal = reservoir.list_uribases (domidx)
	usr = reservoir.list_uribases (domidx, backend=False)
	bck = reservoir.list_uribases (domidx, enduser=False)
	nicelist (hd + ' [enduser]', usr)
	nicelist (hd + ' [backend]', bck)
	nicelist (hd + ' [listall]', lal)
	print ()

nicelist ('Initially empty', reservoir.list_uribases (domidx))

# Follow the LDAP example in doc/CANONICAL-URI.MD
#
# objectClass: labeledURIObject
# labeledURI: ftp://ftp Retro File Transfer Protocol
# labeledURI: https://reservoir/reservoir/raw?apphint=music Listen to Your Music
# labeledURI: https:///?apphint=music&domain=orvelte.nep
# labeledURI: amqps://amqp:1234/reservoir?apphint=inbox


triplelist ('START/EMPTY')

reservoir.add_uribase (domidx, 'ftp://ftp', 'Retro File Transfer Protocol')
triplelist ('ftp+')

reservoir.add_uribase (domidx, 'https://reservoir/reservoir/raw?apphint=music', 'Listen to Your Music')
triplelist ('ftp+, raw+')

reservoir.add_uribase (domidx, 'https:///?apphint=music&domain=orvelte.nep')
triplelist ('ftp+, raw+, dom-')

reservoir.add_uribase (domidx, 'amqps://amqp:1234/reservoir?apphint=inbox')
triplelist ('ftp+, raw+, dom-, msg-')

reservoir.del_uribase (domidx, 'https://reservoir/reservoir/raw?apphint=music')
triplelist ('ftp+, dom-, msg-')

reservoir.del_uribase (domidx, 'amqps://amqp:1234/reservoir?apphint=inbox')
triplelist ('ftp+, dom-')

reservoir.del_uribase (domidx, 'ftp://ftp')
triplelist ('dom-')

reservoir.del_uribase (domidx, 'https:///?apphint=music&domain=orvelte.nep')
triplelist ('FINISH/EMPTY')

