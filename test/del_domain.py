#!/usr/bin/env python3
#
# Store data in a Resource, and dig it back up.  Also check files.


from arpa2 import reservoir

domain = '%s.nep' % reservoir.random_uuid ().split ('-') [0]

clx = reservoir.add_domain (domain)
clx.use_apphint ()

reservoir.add_collection (clx,'Storage test')

#L8R# reservoir.del_collection (clx)

#L8R# reservoir.del_collection (clx)

reservoir.del_domain (domain, recursive=True)

#L8R# meta = {
#L8R# 	'objectClass': ['reservoirResource'],
#L8R# 	'mediaType': 'text/plain',
#L8R# 	'description': 'Some more text would be nice, but it would require an awful amount of creativity.  Momentarily, it feels like the desired entropy for that fails me.',
#L8R# 	'uniqueIdentifier': 'tra-la-la...',
#L8R# }
#L8R# rsc = reservoir.add_resource (clx, **meta)
#L8R# 
#L8R# print ('Resource typed', type (rsc))
#L8R# print ('Resource:', rsc)
#L8R# 
#L8R# txt = rsc.open ().read ()
#L8R# 
#L8R# print ('Text   #%d:' % len(txt), txt)
#L8R# 
#L8R# tf = rsc.open (writing=True, binary=False)
#L8R# tf.write ('Hello World')
#L8R# tf.close ()
#L8R# rsc.commit ()
#L8R# 
#L8R# txt = rsc.open ().read ()
#L8R# 
#L8R# print ('Text   #%d:' % len(txt), txt)
#L8R# 
#L8R# rsc2 = reservoir.clone_resource (rsc, clx, 'troe-loe-loe!!!')
#L8R# 
#L8R# txt2 = rsc2.open ().read ()
#L8R# 
#L8R# print ('Cloned #%d:' % len(txt2), txt2)
#L8R# 
#L8R# reservoir.add_collection (clx, 'Sidepass test')
#L8R# reservoir.move_resource (rsc2, clx, 'truu-lu-lu???')
#L8R# # We (may have) changed the name
#L8R# rsc2.commit ()
#L8R# 
#L8R# txt3 = rsc2.open ().read ()
#L8R# print ('Moved  #%d:' % len(txt3), txt3)
#L8R# 
#L8R# blurb = reservoir.search_resources (clx, '(uniqueIdentifier=*blurb)')
#L8R# print ('Blurb:', blurb)
#L8R# print ('Blurb.keys():', blurb.keys ())
#L8R# for (k,bi) in blurb.items ():
#L8R# 	print ('Blurb[k]:', bi)
#L8R# 	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
#L8R# 	bi ['uniqueIdentifier'] = ['Nou, nou!']
#L8R# 	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
#L8R# 	bi.commit ()
#L8R# 	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
#L8R# 	bi.rollback ()
#L8R# 	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
#L8R# 
#L8R# print ('Blurbless:', reservoir.search_resources (clx, '(uniqueIdentifier=*blurbless)'))
#L8R# 
#L8R# 
