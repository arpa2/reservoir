#!/usr/bin/env python

from arpa2 import reservoir

domain = '%s.nep' % reservoir.random_uuid ().split ('-') [0]

idx = reservoir.add_domain (domain)
print ('Domain', domain, 'index', idx)

idx2 = reservoir.get_domain (domain)
print ('Get it again; index', idx2)

print ('List of domains:', reservoir.list_domains ())

usr = reservoir.add_domain_user (domain, 'charlie')
print ('User', usr)

usr2 = reservoir.get_domain_user (domain, 'charlie')
print ('Get it again; user', usr2)

print ('List of domain users (from str):', reservoir.list_domain_users (domain))
print ('List of domain users (from idx):', reservoir.list_domain_users (idx))

reservoir.add_collection (idx, 'Collection for Domain')
print ('Collection for Domain:', idx)

reservoir.add_collection (usr, 'Collection for User')
print ('Collection for User:', usr)

usr_uuid = usr.get_colluuid ()
print ('UUID for User:', usr_uuid)
idx.set_colluuid (usr_uuid)
print ('Moved to Domain:', idx)
