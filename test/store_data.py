#!/usr/bin/env python3
#
# Store data in a Resource, and dig it back up.  Also check files.


from arpa2 import reservoir

domain = '%s.nep' % reservoir.random_uuid ().split ('-') [0]

clx = reservoir.add_domain (domain)
clx.use_apphint ()

reservoir.add_collection (clx,'Storage test')

meta = {
	'objectClass': ['reservoirResource'],
	'mediaType': 'text/plain',
	'description': 'Some more text would be nice, but it would require an awful amount of creativity.  Momentarily, it feels like the desired entropy for that fails me.',
	'uniqueIdentifier': 'tra-la-la...',
}
rsc = reservoir.add_resource (clx, **meta)

print ('Resource typed', type (rsc))
print ('Resource:', rsc)

txt = rsc.open ().read ()

print ('Text   #%d:' % len(txt), txt)

tf = rsc.open (writing=True, binary=False)
tf.write ('Hello World')
tf.close ()
rsc.commit ()

txt = rsc.open ().read ()

print ('Text   #%d:' % len(txt), txt)

rsc2 = reservoir.clone_resource (rsc, clx, 'troe-loe-loe!!!')

txt2 = rsc2.open ().read ()

print ('Cloned #%d:' % len(txt2), txt2)

reservoir.add_collection (clx, 'Sidepass test')
reservoir.move_resource (rsc2, clx, 'truu-lu-lu???')
# We (may have) changed the name
rsc2.commit ()

txt3 = rsc2.open ().read ()
print ('Moved  #%d:' % len(txt3), txt3)

blurb = reservoir.search_resources (clx, '(uniqueIdentifier=*blurb)')
print ('Blurb:', blurb)
print ('Blurb.keys():', blurb.keys ())
for (k,bi) in blurb.items ():
	print ('Blurb[k]:', bi)
	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
	bi ['uniqueIdentifier'] = ['Nou, nou!']
	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
	bi.commit ()
	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])
	bi.rollback ()
	print ('Blurb[k].ID:', bi ['uniqueIdentifier'])

print ('Blurbless:', reservoir.search_resources (clx, '(uniqueIdentifier=*blurbless)'))


