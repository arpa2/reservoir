#!/usr/bin/env python3
#
# Map a URI from base to full to base and expect the same
#
# Note: This is expected to fail under empty delimiters, but that's no problem


from arpa2 import reservoir


# Base URI examples from the doc:
#
# objectClass: labeledURIObject
# labeledURI: ftp://ftp Retro File Transfer Protocol
# labeledURI: https://reservoir/reservoir/raw?apphint=music Listen to Your Music
# labeledURI: https:///?apphint=music&domain=orvelte.nep
# labeledURI: amqps://amqp:1234/reservoir?apphint=inbox
#
bases = [
	'ftp://ftp',
	'https://reservoir/reservoir/raw?apphint=music',
	'https:///?apphint=music&domain=orvelte.nep',
	'amqps://amqp:1234/reservoir?apphint=inbox',
]

# Full URI examples from the doc, matching the above base URIs:
# ftp://ftp.orvelte.nep/colluuid/resuuid
# https://reservoir.orvelte.nep/reservoir/raw/colluuid/resuuid?apphint=music
# https://orvelte.nep/colluuid/resuuid?domain=orvelte.nep
# amqps://amqp.orvelte.nep:1234/reservoir/colluuid/resuuid?apphint=inbox
#
expected_full = [
	'ftp://ftp.orvelte.nep/%(colluuid)s/%(resuuid)s',
	'https://reservoir.orvelte.nep/reservoir/raw/%(colluuid)s/%(resuuid)s?apphint=music',
	'https://orvelte.nep/%(colluuid)s/%(resuuid)s?apphint=music&domain=orvelte.nep',
	'amqps://amqp.orvelte.nep:1234/reservoir/%(colluuid)s/%(resuuid)s?apphint=inbox',
	
]

domain = 'orvelte.nep'
colluuid = reservoir.random_uuid ()
resuuid = reservoir.random_uuid ()

for (base,full_patn) in zip (bases,expected_full):

	print ('#\n# NEW TEST; COLLUUID ONLY.  PAIRED LINES SHOULD MATCH.\n#')

	# Prepare.  Only colluuid.
	#
	full = full_patn % { 'colluuid':colluuid, 'resuuid':'', }
	#DEBUG# print ('TEST BASE:', base)
	#DEBUG# print ('TEST FULL:', full)
	#DEBUG# print ()

	# Compute.  Only colluuid.
	#
	b2f   = reservoir.uri_basetofull (base, domain, colluuid)
	b2f2b = reservoir.uri_fromfull (full, base)

	# Print.  Only colluuid.
	#
	print ('Canonical:  //%s/%s/' % (domain,colluuid))
	print ()
	print ('Original:   %s' % base)
	print ()
	print ('Expected:   %s' % full)
	print ('Computed:   %s' % b2f)
	print ()
	print ('Revamped:   //%s/%s/%s' % (b2f2b[0],b2f2b[1],b2f2b[2] or ''))
	print ('Canonical:  //%s/%s/' % (domain,colluuid))
	print ()

	print ('#\n# NEW TEST; COLLUUID AND RESUUID.  PAIRED LINES SHOULD MATCH.\n#')

	# Prepare.  With resuuid.
	#
	full = full_patn % { 'colluuid':colluuid, 'resuuid':resuuid, }
	#DEBUG# print ('TEST BASE:', base)
	#DEBUG# print ('TEST FULL:', full)
	#DEBUG# print ()

	# Compute.  With resuuid.
	#
	b2f   = reservoir.uri_basetofull (base, domain, colluuid, resuuid)
	b2f2b = reservoir.uri_fromfull (full, base)

	# Print.  Only colluuid.
	#
	print ('Canonical:  //%s/%s/%s' % (domain,colluuid,resuuid))
	print ()
	print ('Original:   %s' % base)
	print ()
	print ('Expected:   %s' % full)
	print ('Computed:   %s' % b2f)
	print ()
	print ('Revamped:   //%s/%s/%s' % (b2f2b[0], b2f2b[1],b2f2b[2] or ''))
	print ('Canonical:  //%s/%s/%s' % (domain,colluuid,resuuid))
	print ()

