#!/usr/bin/env python3
#
# Test the Remote Magnetic Tape simulation in arpa2.reservoir.rmt.


import os
import os.path
from os import environ
import sys
import time


from arpa2 import reservoir


# Reservoir's app hint for RMT
#
rmt_apphint = 'magtape'


domain = '%s.tape' % reservoir.random_uuid ().split ('-') [0]
user = 'whoami'
environ ['REMOTE_USER'] = '%s@%s' % (user,domain)
domidx = reservoir.add_domain (domain)
reservoir.add_uribase (domidx, 'ftp://ftp/backups/tape/mt0', 'Download')
idx = reservoir.add_domain_user (domain, user)
reservoir.add_collection (idx, rmt_apphint)
reservoir.add_collection (idx, '/dev/mt0')


here = os.path.dirname (sys.argv [0])
path_rmt = here + '/../src/reservoir/rmt.py'

with os.popen (path_rmt, 'w') as rmt:

	print ('Opening device /dev/mt0 in O_RDWR mode')
	rmt.write ('O/dev/mt0\n3 O_RDWR\n')
	rmt.flush ()
	time.sleep (1)

	print ('Writing "Hello World...\\n" (15 bytes)')
	rmt.write ('W15\nHello World...\n')
	rmt.flush ()
	time.sleep (1)

	print ('Spooling back to position 0 from the file start')
	rmt.write ('L0\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading back 15 bytes and showing you the output')
	rmt.write ('R15\n')
	rmt.flush ()
	time.sleep (1)

	print ('Closing the device')
	rmt.write ('C\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reopening device /dev/mt0 in O_RDWR mode')
	rmt.write ('O/dev/mt0\n3 O_RDWR\n')
	rmt.flush ()
	time.sleep (1)

	print ('Tell the server we are using the "modern" version 1 of 1989')
	rmt.write ('I-1\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Skipping ahead to the 5th file')
	rmt.write ('I1\n4\n')
	rmt.flush ()
	time.sleep (1)

	print ('Scribbling on the wall')
	rmt.write ('W35\nHey! Teacher! Leave us kids alone!\n')
	rmt.flush ()
	time.sleep (1)

	print ('Rewinding the tape')
	rmt.write ('I5\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading the first word, 5 bytes')
	rmt.write ('R5\n')
	rmt.flush ()
	time.sleep (1)
	print (' <--- spontaneously inserted newline (and space, arrow, comment)')

	print ('Writing an EOF mark at the current tape position')
	rmt.write ('I0\n1\n')
	rmt.flush ()
	time.sleep (1)

	print ('That set us in the next file; going back one file')
	rmt.write ('I2\n1\n')
	rmt.flush ()
	time.sleep (1)

	print ('Seek the start of file')
	rmt.write ('L0\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Trying to read all that the file has')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Moving to 3rd file')
	rmt.write ('I1\n2\n')
	rmt.flush ()
	time.sleep (1)

	print ('Writing content')
	rmt.write ('W18\nA Whole New World\n')
	rmt.flush ()
	time.sleep (1)

	print ('Skipping to 5th file')
	rmt.write ('I1\n2\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading the writings off the wall')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Skipping back to 3rd file')
	rmt.write ('I2\n2\n')
	rmt.flush ()
	time.sleep (1)

	print ('Taking a hike on a magic carpet')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

with os.popen (path_rmt, 'w') as rmt:

	print ('Reopening device /dev/mt0 in O_RDWR mode')
	rmt.write ('O/dev/mt0\n3 O_RDWR\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading what remains in the current file (should be nothing, at tape end)')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Skipping back to 5th file')
	rmt.write ('I2\n1\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading what is in this last, 5th, file on the tape')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Tell the server we are using the "modern" version 1 of 1989')
	rmt.write ('I-1\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Erasing the tape (meaning, increment the generation)')
	rmt.write ('i3\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading what is in the first, empty, file on the tape')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Writing about a fresh new wind')
	rmt.write ('W40\nNew brooms usually claim to sweep clean\n')
	rmt.flush ()
	time.sleep (1)

with os.popen (path_rmt, 'w') as rmt:

	print ('Reopening device /dev/mt0 in O_RDWR mode')
	rmt.write ('O/dev/mt0\n3 O_RDWR\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading what remains in the current file (should be nothing, at tape end)')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

	print ('Rewinding the tape')
	rmt.write ('I5\n0\n')
	rmt.flush ()
	time.sleep (1)

	print ('Reading what is in the first file on the tape; cleanly after erasing')
	rmt.write ('R100\n')
	rmt.flush ()
	time.sleep (1)

