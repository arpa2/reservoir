#!/usr/bin/env python3
#
# InternetWide Communication with Reservoir over AMQP 1.0
#
# These classes represent responsive elements (Containers)
# that are willing to send and receive Resources between
# domains.
#
# This assumes _amqp._tcp SRV records in DNS for a domain,
# pointing to the host and port that will take messages.
#TODO# Separate from _amqps._tcp or not?
#
# The customary setup will run a daemon as a virtual host
# for numerous domains, for which it finds inboxes in the
# Reservoir.  Requesters may however request more specific
# addresses, because everything is controlled with an ACL
# anyway, so there is no fear of opening up the Reservoir.
#
# AMQP uses SASL and is an excellent candidate for realm
# crossover with SXOVER to connect sender and recipient
# domains.  This means that a back-call will be made by
# a trusting server, to the remote's home realm, to
# validate the remote as indeed being a member of that
# realm.  The home realm itself is validated with usual
# mechanisms: DNSSEC, DANE, TLS.
#
# From: Rick van Rein <rick@openfortress.nl>


import sys
import time
import threading

import re
import uuid

import syslog

from proton import Message, Delivery
from proton.handlers import MessagingHandler, Reject
from proton.reactor import Container, ApplicationEvent, EventInjector

from arpa2 import reservoir



# Pattern of a Resource DN
#
rex_resourceDN = '^resource=([^,=]+),(?:resins|resourceInstance)=([^,=]+),associatedDomain=([^,=]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
re_resourceDN = re.compile (rex_resourceDN)



# Be prepared to insert an incoming Message
#
def message2reservoir (msg, domain, user=None, apphint=None):
	inhome = reservoir.Index (domain, user=user, apphint=apphint)
	inhome.use_apphint ()
	inhome.index_name ('Negrin')
	attrs = { }
	for (k,v) in msg.properties.items ():
		k = k [:k.rindex (':')]
		if k not in attrs:
			attrs [k] = [ ]
		attrs [k].append (v)
	print ('Received Attributes: %r' % attrs)
	rsc = reservoir.add_resource (inhome, **attrs)
	print ('Received Resource: %r' % rsc)
	rsc.open (writing=True).write (msg.body)
	rsc.commit ()


# Retrieve a Resource from Reservoir and turn it into a Message.
# The basis for this are the text parts of the Canonical URI.
#
def reservoir2message (domain, colluuid, resuuid):
	outhome = reservoir.Index (domain)
	outhome.set_colluuid (colluuid)
	rsc = outhome.load_resource (resuuid)
	print ('Resource: %r' % rsc)
	for (k,vs) in rsc.items ():
		print (' - %s: %r' % (k,vs))
	properties = { }
	for (k,vs) in rsc.items ():
		if k in ['resource','documentAuthor']:
			continue
		for (i,v) in enumerate (vs):
			# We _always_ add :0 and up, so parsing _will_ work
			properties ['%s:%d' % (k,i)] = v
	print ('Properties: %r' % properties)
	blob = rsc.open ().read ()
	print ('Blob read, size %d' % len (blob))
	msgid = uuid.uuid4 ()
	msg = Message (id=msgid, properties=properties, body=blob)
	print ('Message: %r' % (msg))
	return msg


# dn_lcs_pairs() is a generator function that reads lines per two.
# The first is a DN, the second a lifecycleState attribute value.
# This is the standard "protocol" for lifecycle backends.
# The generator produces (dn,lcs) pairs.
#
def dn_lcs_pairs (stream=None):
	if stream is None:
		stream = sys.stdin
	ln1 = stream.readline ()
	while ln1 != '':
		if ln1 [-1:] == '\n':
			ln1 = ln1 [:-1]
		ln2 = stream.readline ()
		if ln2 == '':
			syslog.syslog (syslog.LOG_ERR, 'Input stream closed; dropping sole DN %s' % ln1)
			break
		if ln2 [-1:] == '\n':
			ln2 = ln2 [:-1]
		yield (ln1,ln2)
		ln1 = stream.readline ()


# ReservoirSender is an event handler used when sending messages
#
class ReservoirSender (MessagingHandler):

	# Setup the new object.
	#
	def __init__ (self):
		super(ReservoirSender,self).__init__ ()
		self.msg = None

	# The event loop has started;
	# have a few coordinates delegated.
	#
	def on_start (self, event):
		print ('Sender: on_start()')
		self.ctr = event.container

	# The sender link has credits, we can start sending;
	# pickup any message to make it happen.
	#
	def on_link_opened (self, event):
		print ('Sender: on_link_opened()')
		msg = self.msg
		self.msg = None
		if msg is not None:
			print (' -> sending message %r' % msg)
			self.snd.send (msg)

	# The message has been rejected by the remote;
	# we could ignore it and await retry, or act on it.
	#
	def on_rejected (self, event):
		print ('Sender: on_rejected()')

	# The message has been accepted by the remote;
	# we should report this positive result.
	#
	def on_accepted (self, event):
		print ('Sender: on_accepted()')

	# The message has been settled by the remote;
	# this wraps up, but is neither positive nor negative.
	#
	def on_settled (self, event):
		print ('Sender: on_settled()')
		pass #TODO#

	# An error occurs with the transport;
	# we can report that to stop the attempts.
	#
	def on_transport_error (self, event):
		print ('Sender: on_transport_offer()')
		pass #TODO#

	# A message offered by the main thread (ApplicationEvent);
	# try to send it on its way.
	#
	def on_sendmsg (self, appevt):
		print ('Sender: on_sendmsg()')
		(domain,colluuid,resuuid,lcs) = appevt.subject
		self.lcs = lcs
		self.msg = reservoir2message (domain=domain, colluuid=colluuid, resuuid=resuuid)
		self.cnx = self.ctr.connect ('localhost:5672')
		self.snd = self.ctr.create_sender (self.cnx, '/InternetWide/arpa2.net/Reservoir/xyz')


# ReservoirReceiver is an event handler used when receiving messages
#
class ReservoirReceiver (MessagingHandler):

	# Setup the new object.
	#
	def __init__ (self):
		super(ReservoirReceiver,self).__init__ ()
		self.msg = None

	def on_start (self, event):
		print ('Receiver: on_start()')
		self.ctr = event.container
		self.cnx = self.ctr.connect ('localhost:5672')
		self.rcv = self.ctr.create_receiver (self.cnx, '/InternetWide/arpa2.net/Reservoir/xyz')

	# A message was received;
	# We should store it in Reservoir;
	# Only then should we settle it.
	def on_message (self, event):
		print ('Receiver: on_message()')
		msg = event.message
		print (' - message:: %r' % type (msg))
		print (' - properties: %r' % msg.properties)
		print (' - body:len: %d' % len (msg.body))
		try:
			message2reservoir (msg, domain='arpa2.org', apphint='Music')
			print ('Success in on_message()')
			self.accept (event.delivery)
		except Exception as e:
			print ('Failure in on_message(): %s' % e)
			self.reject (event.delivery)
			raise Reject (str (e))
		#AUTO# self.accept (event.delivery)
		#AUTO# self.settle (event.delivery)

	# A transport error occurred;
	# We may want to report it.
	def on_transport_error (self, event):
		print ('Received: on_transport_error()')



def receiver (daemon=False):
	#
	# Create a handler from ReservoirReceiver, and use it for a new Container
	#
	rhandler = ReservoirReceiver ()
	rcontainer = Container (rhandler)
	rthread = threading.Thread (target=rcontainer.run)
	rthread.daemon = daemon
	rthread.start ()


def sender ():
	# Create a handler from ReservoirSender, and use it for a new Container
	#
	shandler = ReservoirSender ()
	scontainer = Container (shandler)
	evtinj = EventInjector ()
	scontainer.selectable (evtinj)
	sthread = threading.Thread (target=scontainer.run)
	sthread.daemon = True
	sthread.start ()
	#
	# Iterate over (dn,lcs) pairs from the Pulley Lifecycle backend
	# and trigger sending with ApplicationEvent ('sendmsg') for each.
	#
	for (dn,lcs) in dn_lcs_pairs ():
		try:
			(resuuid,colluuid,domain) = re_resourceDN.match (dn).groups ()
			print ('domain:   %s\ncolluuid: %s\nresuuid:  %s\nlcState:  %s' % (domain,colluuid,resuuid,lcs))
			todo = (domain,colluuid,resuuid,lcs)
			appevt = ApplicationEvent ('sendmsg', subject=todo)
			evtinj.trigger (appevt)
		except Exception as e:
			print ('Exception %s' % e)
			print ('bad.DN:  %s\nLCS:     %s' % (dn,lcs))


if __name__ == '__main__':
	print ('Please call this module\'s sender() or receiver() function')

