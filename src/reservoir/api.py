# arpa2.reservoir -- Functions to work on the Reservoir.
#
# Functions to directly address the LDAP tree for Reservoir,
# as well as the file store.
#
# From: Rick van Rein <rick@openfortress.nl>


import os
import sys
import copy
import fcntl
import time

import shutil
import uuid
import string
import re
import hashlib

import urllib.parse

from configparser import ConfigParser

import ldap
from ldap import MOD_ADD, MOD_DELETE, MOD_REPLACE, MOD_INCREMENT
from ldap import SCOPE_BASE, SCOPE_ONELEVEL, SCOPE_SUBTREE
from ldap import NO_SUCH_OBJECT, ALREADY_EXISTS, NOT_ALLOWED_ON_NONLEAF

import json
try:
	import paho.mqtt.client as mqtt
except ImportError:
	mqtt = None
mqtt_cli = None


#
# Configuration
#
# reservoir_uuid --> fixed for this application, no matter where it runs
# reservoir_base --> fixed for this application, no matter where it runs
# reservoir_home --> setup as [core] file_home
#

reservoir_base = 'ou=Reservoir,o=arpa2.net,ou=InternetWide'


# As assigned a fixed value on http://uuid.arpa2.org
#
# This is used as follows:
#  - in a resourceClass/reservoirIndex objects at
#    associatedDomain=,ou=Reservoir,o=arpa2.net,ou=InternetWide
#  - in resourceInstance/reservoirCollection/reservoirIndex objects
#    with a random UUID representing a Collection UUID located at
#    resins=,associatedDomain=,ou=Reservoir,...
#  - it is _not_ used in reservoirResource objects at
#    resource=,resins=,associatedDomain=,ou=Reservoir,...
#
reservoir_uuid = '904dfdb5-6b34-3818-b580-b9a0b4f7e7a9'


#
# Load the configuration file, normally in the file reservoir_conf.
#

defaults = {
	'file_home': '/var/arpa2/reservoir',
	'server_uri': 'ldapi:///',
	'bind_dn': '',
	'bind_mech': 'gssapi',
	'dumb_user_password': '',
}

conf = ConfigParser (defaults, strict=True)

reservoir_conf = [ '/etc/arpa2/reservoir.conf' ]
conf.read (reservoir_conf)

reservoir_home = conf.get ('core', 'file_home')
ldap_serveruri = conf.get ('ldap', 'server_uri')
ldap_binddn    = conf.get ('ldap', 'bind_dn')
ldap_bindmech  = conf.get ('ldap', 'bind_mech')
ldap_userpw    = conf.get ('ldap', 'dumb_user_password')

if ldap_userpw == '':
	ldap_userpw = None
else:
	sys.stderr.write ('\n#\n# YOU CONFIGURED A PASSWORD, THAT IS INCREDIBLY INSECURE AND DUMB\n# (for testing purposes, it may be easier though.  still, consider gssapi)\n#\n')



# Utility function to produce UUID strings.
#
def random_uuid ():
	"""Produce a random UUID in the lowercase string format.
	"""
	return str (uuid.uuid4 ()).lower ()



#
# UUID pattern matching
#
rex_uuid = '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}'
re_uuid = re.compile ('^%s$' % rex_uuid)


#
# DN pattern matching
#
rex_resource_dn = '^resource=([^,]+),(?:resins|resourceInstance)=([^,]+),associatedDomain=([^,]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
rex_collection_dn =                '^(?:resins|resourceInstance)=([^,]+),associatedDomain=([^,]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
rex_user_dn =                                              '^uid=([^,]+),associatedDomain=([^,]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
rex_domain_dn =                                                         'associatedDomain=([^,]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
rex_index_dn =   '^(?:(?:resins|resourceInstance)=([^,]+),|uid=([^,]+),)?associatedDomain=([^,]+),ou=Reservoir,o=arpa2.net,ou=InternetWide$'
#
re_resource_dn   = re.compile (rex_resource_dn  )
re_collection_dn = re.compile (rex_collection_dn)
re_user_dn       = re.compile (rex_user_dn      )
re_domain_dn     = re.compile (rex_domain_dn    )
re_index_dn      = re.compile (rex_index_dn     )



#
# Canonical URI pattern matching: TODO:domain
#
rex_canonuri_coll = '//(?P<domain>%s)/(?P<colluuid>%s)/'                % ('[^/]+', rex_uuid          )
rex_canonuri_res  = '//(?P<domain>%s)/(?P<colluuid>%s)/(?P<resuuid>%s)' % ('[^/]+', rex_uuid, rex_uuid)
re_canonuri_coll = re.compile ('^%s$' % rex_canonuri_coll)
re_canonuri_res  = re.compile ('^%s$' % rex_canonuri_res )


#
# Initialisation of LDAP
#

# cfgln_re = re.compile ('^([A-Z]+)[ \t]*(.*)$')
# ldapcfg = { }
# try:
#         for cfgln in open ('/etc/ldap/ldap.conf', 'r').readlines ():
#                 m = cfgln_re.match (cfgln)
#                 if m is not None:
#                         (key,val) = m.groups ()
#                         ldapcfg [key] = val
# except:
#         pass

# if not ldap_bindmech == 'gssapi':
# 	ldap_userpw = os.environ.get ('ARPA2_BINDPW')
# 	if ldap_userpw is None:
# 		import getpass
# 		print ('LDAPuser: ', ldap_binddn)
# 		ldap_userpw = getpass.getpass ('Password: ')
# 		os.environ ['ARPA2_BINDPW'] = ldap_userpw


#
# Connect to the LDAP server.
#
dap = ldap.initialize (ldap_serveruri)
if ldap_bindmech == 'gssapi':
	sasl_auth = ldap.sasl.gssapi()
	dap.sasl_interactive_bind_s(ldap_binddn, sasl_auth)
else:
	dap.bind_s (ldap_binddn, ldap_userpw)


#
# Utilities to map str() and bytes()
#
def s2b (s):
	t = type (s)
	if t == bytes:
		return s
	elif t == str:
		return bytes(s,'utf-8')
	elif t == list:
		return [ s2b(s0) for s0 in s ]
	elif t == set:
		# Highly specific for Python-LDAP
		return [ s2b(s0) for s0 in s ]
	elif t == tuple:
		# Highly specific for Python-LDAP
		return tuple([s[0]] + [ s2b(s0) for s0 in s[1:] ])
	elif t == dict:
		return { k:s2b(v) for (k,v) in s.items() }
	else:
		raise TypeError ('s2b got %s' % str(t))

def b2s (b):
	t = type (b)
	if t == str:
		return b
	elif t == bytes:
		return str(b,'utf-8')
	elif t == list:
		return [ b2s(b0) for b0 in b ]
	elif t == set:
		return set([ b2s(b0) for b0 in b ])
	elif t == tuple:
		# Highly specific for Python-LDAP
		return tuple([b[0]] + [ b2s(b0) for b0 in b[1:] ])
	elif t == dict:
		return { k:b2s(v) for (k,v) in b.items() }
	else:
		raise TypeError ('b2s got %s' % str(t))


#
# Broadcast a message over MQTT, relating to subscribed Collections:
#  - MQTT_ADD to introduce a new Resource
#  - MQTT_DEL to signal removal of a Resource (TODO: we do not support deletion yet)
#  - MQTT_CHG for changes to a Resource
#
# Topics always look the same: reservoiruuid/domain/colluuid/resuuid
#
# Subscriptions are always to an existing collection, and only permitted
# to receive publications by clients with K(now-about) rights
#
# A good series of articles about MQTT and paho-mqtt is:
# https://mntolia.com/fundamentals-mqtt/
# https://mntolia.com/mqtt-python-with-paho-mqtt-client/
#
MQTT_ADD = 'ADD'
MQTT_DEL = 'DEL'
MQTT_CHG = 'CHG'

def mqtt_disconnect (*args):
	global mqtt_cli
	mqtt_cli = None

def mqtt_connect ():
	global mqtt_cli
	if mqtt_cli is not None:
		return True
	if mqtt is None:
		# No library; quietly return
		return False
	try:
		mqtt_cli = mqtt.Client ()
		mqtt_cli.connect ('localhost')
	except:
		mqtt_cli = None
		return False
	mqtt_cli.on_disconnect = mqtt_disconnect
	mqtt_cli.loop_start ()
	return True

def mqtt_notify (resource, action):
	global mqtt_cli
	dom = resource.get_domain ()
	clx = resource.get_colluuid ()
	res = resource.get_resuuid ()
	topic = '/InternetWide/arpa2.net/Reservoir/%s/%s/%s' % (dom,clx,res)
	if action == MQTT_DEL:
		message = ''
	else:
		attrs = { k: [ v for v in vs ] for (k,vs) in resource.items () }
		message = json.dumps( { 'info':  attrs, 'canon': '//%s/%s/%s' % (dom,clx,res), } )
	if not mqtt_connect ():
		return
	print ('Sending on topic %s msg %s' % (topic,message))
	mqtt_cli.publish (topic=topic, payload=message, qos=1, retain=True)

# Update .../colluuid/ref/name -> newuuid
#  - colluuid is None => Home Index (possibly unnamed)
#  - newuuid is None  => remove item
#
def mqtt_notify_ref (index, name, newuuid):
	global mtqq_cli
	dom = index.get_domain ()
	clx = index.get_colluuid ()
	if name is not None:
		name = urllib.parse.quote (name)
	if clx is not None and name is not None:
		topic = '/InternetWide/arpa2.net/Reservoir/%s/%s/ref/%s' % (dom,clx,name)
	elif clx is not None:
		return
	elif name is None:
		topic = '/InternetWide/arpa2.net/Reservoir/%s/app'       % (dom,        )
	else:
		topic = '/InternetWide/arpa2.net/Reservoir/%s/app/%s'    % (dom,    name)
	if newuuid is None:
		message = ''
	else:
		message = json.dumps (newuuid)
	if not mqtt_connect ():
		return
	print ('Sending on topic %s msg %s' % (topic,message))
	mqtt_cli.publish (topic=topic, payload=message, qos=2, retain=True)

# Update .../domain.name/base to a new collection of baseURI values
# Supply the new/current list of (uri,label) pairs, where the
# and their optional labels.  During lifetime of a domain, uribases
# is always a list (possibly empty) and when deleting a domain it
# is set to None to remove any retained messages for the base.
#
def mqtt_notify_uribases (domain, uribases):
	topic = '/InternetWide/arpa2.net/Reservoir/%s/base' % domain
	# JSON already converts the (,) to [,] and map None to null :)
	if uribases is not None:
		message = json.dumps (uribases)
	else:
		message = ''
	if not mqtt_connect ():
		return
	print ('Sending on topic %s msg %s' % (topic,message))
	mqtt_cli.publish (topic=topic, payload=message, qos=2, retain=True)


#
# Classes
#


class Resource:
	"""Cache the LDAP contents of a Reservoir Resource Object.
	   The various attributes can be inspected as desired.
	   #TODO# support collecting and then saving state to LDAP
	   #TODO# have a form for a new/empty Resource
	"""
	def __init__ (self, indexobj, resuuid, preloaded_attrs=None):
		assert resuuid  is not None, 'Provide a Resource UUID'
		assert indexobj is not None, 'Provide a Collection object'
		self.resuuid  = resuuid
		self.indexobj = indexobj
		self.colluuid = indexobj.get_colluuid ()
		self.domain   = indexobj.get_domain ()
		self.dn = 'resource=%s,resins=%s,associatedDomain=%s,%s' % (resuuid,self.colluuid,self.domain,reservoir_base)
		if preloaded_attrs is None:
			fl1 = '(objectClass=reservoirResource)'
			try:
				#DEBUG# print ('searching', dn1, fl1, al1)
				qr1 = dap.search_s (self.dn, SCOPE_BASE, filterstr=fl1)
				#DEBUG# print ('Query response:', qr1)
			except NO_SUCH_OBJECT:
				return KeyError ('Resource %s not found in collection %s of %s' % (resuuid,self.colluuid,self.domain))
			assert len(qr1) == 1, 'Query lists unexpected elements'
			[(dn,at)] = qr1
		else:
			at = preloaded_attrs
		self.meta = { k:set (b2s (vs)) for (k,vs) in at.items () }
		assert len (self.meta.get ('uniqueIdentifier', [])) == 1, 'There must be exactly one uniqueIdentifier'
		self.olds = self.pairs ()
		self._dirty_hashes = 'documentHash' not in self.meta
		self._dirty        = self._dirty_hashes

	@classmethod
	def from_canonical_uri (cls, canonical_uri):
		"""Construct a Resource object from the provided canonical_uri string.
		"""
		m = re_canonuri_res.match (canonical_uri)
		assert m is not None, 'Syntax error in Canonical URI of Resource'
		(domain,colluuid,resuuid) = m.groups ()
		clx = Index (domain)
		clx.set_colluuid (colluuid)
		return cls (clx, resuuid)

	def dn_resource (self):
		"""Return the current DN of this Resource object.
		"""
		return self.dn

	def get_resuuid (self):
		"""Return the UUID for this Resource object.
		"""
		return self.resuuid

	def get_colluuid (self):
		"""Return the UUID for the Collection holding this Resource.
		"""
		return self.colluuid

	def set_colluuid (self, colluuid):
		"""Change the UUID for the Collection holding this Resource.
		   This is used during move_resource().
		"""
		self.colluuid = colluuid
		self.dn = 'resource=%s,resins=%s,associatedDomain=%s,%s' % (self.resuuid,self.colluuid,self.domain,reservoir_base)

	def get_domain (self):
		"""Return the domain name holding this Resource.
		"""
		return self.domain

	def get_canonical_uri (self, domain_relative=False):
		if relative:
			return  '/%s/%s'    % (self.colluuid,            self.resuuid)
		else:
			return '//%s/%s/%s' % (self.domain,self.colluuid,self.resuuid)

	def __del__ (self):
		"""When the object is cleaned up, we consider it too late
		   to save the state to LDAP.  But we can still moan about
		   being left behind.
		"""
		if self._dirty:
			sys.stderr.write ('\n\n### LOOSING CACHED RESOURCE STATE: %s\n\n' % self.dn)

	def __str__ (self):
		return '%r @%s' % (self.meta,self.dn)

	def keys (self):
		return self.meta.keys ()

	def values (self):
		return self.meta.values ()

	def items (self):
		"""Return the key-to-values mapping from LDAP.  To have a
		   unique key, the values are offered as a set, even for
		   singular attribute types.  See pairs() for an alternative
		   that does not perform grouping.
		   on the sets returned, as they are shared with the primary
		   source of this information.
		"""
		return self.meta.items ()

	def pairs (self):
		"""Return type-value pairs from LDAP, possibly with repeated
		   types.  This is different from items() which models the
		   type as a key, and so must group values into one set.
		   
		   TODO: The following would be nice but it WONTWORK yet:
		   The returned list is pretty clever; you can add and remove
		   items as you would expect, and the attributes change too.
		"""
		#WONTWORK# meta = self.meta
		#WONTWORK# class Pairs (list):
		#WONTWORK# 	def __add__ (self, kv):
		#WONTWORK# 		global meta
		#WONTWORK# 		print ('Pairs.__add__(%r)' % kv)
		#WONTWORK# 		(k,v) = kv
		#WONTWORK# 		list.__add__ (self, kv)
		#WONTWORK# 		if not k in meta:
		#WONTWORK# 			meta [k] = set([v])
		#WONTWORK# 		else:
		#WONTWORK# 			meta [k].add (v)
		#WONTWORK# 	def __delitem__ (self, kv):
		#WONTWORK# 		global meta
		#WONTWORK# 		print ('Pairs.__delitem__(%r)' % kv)
		#WONTWORK# 		(k,v) = kv
		#WONTWORK# 		list.__delitem__ (self, kv)
		#WONTWORK# 		meta [k].discard (v)
		#WONTWORK# return Pairs ([ (typ,val)
		#WONTWORK#          for typ in meta.keys ()
		#WONTWORK#          for val in meta [typ] ])
		return set ([ (typ,val)
		         for typ in self.meta.keys ()
		         for val in self.meta [typ] ])

	def get_index (self):
		"""Return the index object under which this Resource resides.
		"""
		return self.indexobj

	def get (self, key, default=None):
		return self.meta.get (key, default)

	def compute_hashes (self):
		"""Recompute the secure hashes of this document.  This is
		   an expensive operation, so it is normally delayed until
		   commit() is called.  To trigger at at this time, call
		   dirty_hashes().  This computation will set the values
		   for documentHash in memory, but still requires commit()
		   to save it to LDAP.
		"""
		class SizeHashing:
			def __init__ (self):
				self.totlen = 0
			def update (self, blk):
				self.totlen += len (blk)
			def hexdigest (self):
				#HEXBYTES# h = '0%x' % self.totlen
				#DEBUG# print ('Length %d, hashed to %s' % (self.totlen,h[len(h)%2:]))
				#HEXBYTES# return h [len(h)%2:]
				return '%d' % (self.totlen,)
		#DEBUG# print ('Computing hashes')
		totlen = 0
		hashes = {
			'_size': SizeHashing (),
			'sha256': hashlib.sha256 (),
			'sha3_256': hashlib.sha3_256 (),
			# ...making all your wishes come true...
		}
		with self.open () as fh:
			block = fh.read (10240)
			while len (block) > 0:
				totlen += len (block)
				for k in hashes.values ():
					k.update (block)
				block = fh.read (10240)
		self ['documentHash'] = [
			'%s %s' % (k,v.hexdigest().lower())
			for (k,v) in hashes.items () ]
		#DEBUG# print ('Set documentHash to %r', self ['documentHash'])

	def dirty_hashes (self):
		"""Ensure a call to compute_hashes() as part of the next
		   commit().  The old hash values are discredited and
		   discarded, as they should not longer be trusted.  The
		   memory state is now considered dirty, meaning that a
		   call to commit() will be required for proper operation.
		"""
		self._dirty_hash = True
		self._dirty = True
		del self ['documentHash']

	def rollback (self):
		"""Deliberately forget meta data changes on this object.
		   This is part of an orderly return to an old state.
		   This is the opposite of commit(), which writes any
		   collected changes to LDAP.
		   
		   Note: This *only* rolls back meta data, not changes
		         to the resource.  Since those impact hashes,
		         it may still be required to commit() after
		         this call.  That's the result of automatic
		         computation of these hashes.
		"""
		self.meta = { }
		for (k,v) in self.olds:
			if not k in self.meta:
				self.meta [k] = set ()
			self.meta [k].add (v)
		self._dirty = False

	def commit (self):
		"""Commit changes by sending them to LDAP.  When the
		   stored values have changed since this object cached
		   changes, it may happen that conflicts arise, which
		   will be reported by this call.  This method raises
		   an Exception when multiple uniqueIdentifier values
		   are being set.
		"""
		assert len (self.meta.get ('uniqueIdentifier', [])) == 1, 'There must be exactly one uniqueIdentifier'
		if not self._dirty:
			return
		if self._dirty_hashes:
			self.compute_hashes ()
		olds = self.olds
		news = self.pairs ()
		dn1 = self.dn_resource ()
		at1 = [
			(MOD_ADD   ,k,s2b(v)) for (k,v) in news-olds
		] + [
			(MOD_DELETE,k,s2b(v)) for (k,v) in olds-news
		]
		#DEBUG# print ('ABOUT TO MODIFY %r' % at1)
		if at1 != []:
			dap.modify_s (dn1, at1)
		self.olds = self.pairs ()
		self._dirty = False
		mqtt_notify (self, MQTT_CHG)

	def __contains__ (self, k):
		return self.meta.__contains__ (k)

	def __getitem__ (self, k):
		return self.meta [k]

	def __setitem__ (self, k, vs):
		self.meta [k] = set (vs)
		self._dirty = True

	def __delitem__ (self, k):
		if k in self.meta:
			del self.meta [k]
		self._dirty = True

	def open (self, reading=True, writing=False, locked=True,
	                binary=True, truncate=False, endpos=False):
		"""Return the file underneath this resource.  The
		   returned object is file-like and offers at least
		   the functions read, write, seekable/seek/tell
		   and of course, close.
		   
		   The file must be opened for reading and/or writing.
		   Creating and truncating only works when writing.
		   Locking while writing is exclusive, otherwise it
		   is shared; the lock is filesystem-operated and
		   is given up when closing; unrelated to LDAP lockedBy.
		   When endpos is set, the file pointer is at the end.
		"""
		devnull = False
		lockmode = fcntl.LOCK_SH
		if writing:
			flags = 'w' if truncate else 'r+'
			lockmode = fcntl.LOCK_EX
		else:
			assert not truncate, 'You can only truncate a Resource in write mode'
		if reading and not writing:
			flags = 'r'
		if binary:
			flags = flags [:1] + 'b' + flags [1:]
		fn = '%s/%s/%s/%s' % (reservoir_home,self.domain,self.colluuid,self.resuuid)
		fh = open (fn, flags, 0o644)
		if locked:
			fd = fh.fileno ()
			try:
				fcntl.lockf (fd, lockmode | fcntl.LOCK_NB)
			except:
				fh.close ()
				raise
		if writing:
			self.dirty_hashes ()
		return fh

	def sendto (self, recpt, sender=None, topic='', timeout=None):
		"""Schedule sending the resource to indicitad recipient(s)
		   in recpt.  The topic on the destination is given, and
		   may be a domain-relative Canonical URI or a path that
		   starts with an apphint.  An override to the default
		   timeout may be suggested, and will be used to schedule
		   exponential-backoff delivery attempts.  The timeout is
		   specified in seconds; if it is before the current time
		   (seconds since epoch) then it is considered relative to
		   the current time.  It will be clipped if it goes too
		   far in the future.  The sender may be set within the
		   constraints of identity inheritance and defaults to the
		   autenticated identity, and must have suitable permission.
		   
		   This works by setting up a lifecycleState attribute in
		   LDAP, which in turn triggers the sending daemon.
		"""
		if sender is None:
			#TODO# Derive from authenticated identity (whoami)
			sender = 'tommy@sesamstraat.nep'
		self.indexobj.check_acl (sender)
		now = int (time.time ())
		if type (timeout) != int:
			timeout = 86400
		if timeout <= 86400:
			timeout += now
		if timeout > now + 86400:
			timeout = now + 86400
		if type (recpt) != list:
			recpt = [recpt]
		at1 = [ (MOD_ADD,'lifecycleState',
		                 bytes ('init@%d from=%s to=%s . send expire@%d remove' % (now,sender,rcp,timeout), 'utf-8'))
		        for rcp in recpt ]
		dn1 = self.dn_resource ()
		if at1 != []:
			dap.modify_s (dn1, at1)

class Index:
	"""Operations to proceed from index to index while traversing
	   the Reservoir.  Calls update the object, but copies may be
	   taken at any time.
	"""

	def __init__ (self, domain, user=None, apphint=None):
		"""Create an initial index object for a domain and
		   possible a user under that domain.  The initial
		   apphint is set, but not used yet.
		"""
		#TODO#ACL#
		assert domain is not None, 'You need a domain, and optionally a user, to find a Reservoir home node'
		self.domain   = domain
		self.user     = user
		self.colluuid = None
		self.curidx   = None
		self.set_apphint (apphint)

	@classmethod
	def from_canonical_uri (cls, canonical_uri):
		"""Construct an Index object from the provided canonical_uri string.
		"""
		m = re_canonuri_coll.match (canonical_uri)
		assert m is not None, 'Syntax error in Canonical URI of Collection'
		(domain,colluuid) = m.groups ()
		clx = cls (domain)
		clx.set_colluuid (colluuid)
		return clx

	@classmethod
	def from_dn (cls, dn):
		"""Construct an Index object from the provided DN string.
		   This works for the following DN patterns:
		   
		   resins=,associatedDomain=,ou=Reservoir,o=arpa2.net,ou=InternetWide
		      uid=,associatedDomain=,ou=Reservoir,o=arpa2.net,ou=InternetWide
		           associatedDomain=,ou=Reservoir,o=arpa2.net,ou=InternetWide
		"""
		m = re_index_dn.match (dn)
		if m is None:
			raise Exception ('No index at %s' % dn)
		(colluuid,user,domain) = m.groups ()
		clx = cls (domain, user=user)
		if colluuid is not None:
			clx.set_colluuid (colluuid)
		return clx

	def __str__ (self):
		dn = 'associatedDomain=%s,%s' % (self.domain,reservoir_base)
		if self.colluuid is not None:
			dn = 'resins=%s,%s' % (self.colluuid,dn)
		elif self.user is not None:
			dn = 'uid=%s,%s' % (self.user,dn)
		return dn

	def check_acl (self, colluuid):
		# raise NotImplementedError ('No ACL support yet')
		sys.stderr.write ('\nNOT IMPLEMENTED: ACL SUPPORT FOR RESERVOIR INDEXES\n\n')

	def set_colluuid (self, colluuid):
		"""For the current domain, switch to a fixed collection
		   by setting its UUID.  Drop a cached object, if any;
		   if the caller cared, he should have copied us first.
		"""
		assert colluuid is not None, 'Collection UUID cannot be None'
		self.check_acl (colluuid)
		self.colluuid = colluuid
		self.curidx = None

	def get_canonical_uri (self, domain_relative=False):
		if relative:
			return  '/%s/'    % (self.colluuid,           )
		else:
			return '//%s/%s/' % (self.domain,self.colluuid)

	def dn_base (self):
		"""Return the baseDN for the Reservoir tree.
		"""
		return reservoir_base

	def dn_domain (self):
		"""Return the DN of the domain home object.
		"""
		dn = 'associatedDomain=%s,%s' % (self.domain, reservoir_base)
		return dn

	def dn_domain_user (self):
		"""Return the DN of the user home object under the domain.
		"""
		assert self.user is not None, 'Not an index for a user'
		dn = self.dn_domain ()
		dn = 'uid=%s,%s' % (self.user, dn)
		return dn

	def dn_home (self):
		"""Return the DN of the home object, either the domain or
		   user under the domain.
		"""
		if self.user is not None:
			return self.dn_domain_user ()
		else:
			return self.dn_domain ()

	def dn_cursor (self):
		"""Return the current DN of this object.
		"""
		return str (self)

	def get_domain (self):
		"""Return the domain set for this LDAP object.
		"""
		return self.domain

	def get_user (self):
		"""Return the user set for this LDAP object, or None.
		   Note that the user may not actually be part of the
		   current path at all.
		"""
		return self.user

	def get_colluuid (self):
		"""Return the collection UUID that this cursor currently
		   points at.
		"""
		return self.colluuid

	def reset_home (self):
		"""Return to the home object, basically the domain or
		   its user object.  The result is a Home Index, which
		   has not had its apphint applied.  You may change it
		   with set_apphint() before use_apphint() is called,
		   the latter of which is automatically done by calls
		   that cannot work from the Home Index.
		"""
		self.check_acl (None)
		self.colluuid = None
		self.curidx = None

	def _index_load (self):
		"""Load the current index and cache it.
		"""
		self.curidx = [ ]
		try:
			dn1 = str (self)
			fl1 = '(objectClass=reservoirIndex)'
			al1 = ['reservoirRef']
			#DEBUG# print ('searching', dn1, fl1, al1)
			qr1 = dap.search_s (dn1, SCOPE_BASE, filterstr=fl1, attrlist=al1)
			#DEBUG# print ('Query response:', qr1)
			for (dn,at) in qr1:
				if 'reservoirRef' in at:
					self.curidx += b2s (at ['reservoirRef'])
		except NO_SUCH_OBJECT:
			pass

	def _index_lookup (self, _name, current_colluuid=None):
		"""Lookup the space-prefixed name or empty string and
		   return the resulting colluuid.  When provided, the
		   search starts from the given colluuid.
		   #TODO# Perhaps express through __getitem__
		"""
		if self.curidx is None:
			self._index_load ()
		#DEBUG# print ('DEBUG: Type of _name is %r' % type (_name))
		_name = _name
		#DEBUG# print ('DEBUG: Looking for "%s" in current index %r' % (_name,self.curidx))
		for idx in self.curidx:
			#DEBUG# print ('DEBUG: Comparing against "%s"' % idx [36:])
			if idx [36:] == _name:
				current_colluuid = idx [:36]
				self.check_acl (current_colluuid)
				self.colluuid = current_colluuid
				self.curidx = None
				#DEBUG# print ('DEBUG: Found %s' % current_colluuid)
				return current_colluuid
		return None

	def index_default (self):
		"""Lookup the default entry in the current index.
		"""
		colluuid = self._index_lookup ('')
		if colluuid is None:
			raise KeyError ('No unnamed/default entry in %s' % str(self))
		self.check_acl (colluuid)
		self.colluuid = colluuid
		self.curidx = None

	def set_apphint (self, apphint):
		"""Setup or change the apphint for this Index.  It may
		   have been setup during initialisation already.  The
		   value None uses the default/unnamed apphint, if it
		   exists, and otherwise causes an exception.
		   
		   Apphints are the step from a Home Index, which is
		   not a Collection, to a real Collection.  They are
		   useful in separating different kinds of application,
		   such as keeping Collections "music" and "finance"
		   separated.  An apphint is not specific for a client
		   but for a kinds of Resources and Collections and a
		   related ACL.
		   
		   When attempting to access a Collection when in a
		   Home Index (for a domain or user), the Apphint
		   guides the way to the new location.  After that it
		   is no longer useful.
		"""
		self.apphint = apphint

	def use_apphint (self):
		"""If this Index is a Home Index, use the apphint to
		   move it to the first Collection.  Quietly return if
		   there already is a current Collection, which is a
		   sign that the current Index is not a Home Index.
		   
		   The apphint is setup during initialisation or with
		   the set_apphint() method.  It is normally applied
		   quietly, but in rare circumstances it can be useful
		   to do it explicitly, notably before generating data
		   from this object, via get_colluuid() or so.
		   
		   There are a few ways of using an apphint, in order:
		   
		    0. If this is not a Home Index, silently return.
		   
		    1. If the apphint is defined in this Home Index,
		       resolve the name to a Collection and return.
		   
		    2. If the domain is setup with the apphint, clone
		       the targeted Collection to a new LDAP object
		       (being smart about attributes, making changes)
		       and reference it from the Home Index with the
		       apphint as its name.  Set the new Collection as
		       the current one, and return.
		       
		       Note how this procedure always fails for domain's
		       Home Indexes, but is very useful for a user's.
		       
		    3. If this Home Index has an unnamed/default
		       apphint, set it as the current Collection and
		       return.
		       
		    4. Having no further options left, raise an error.
		   
		   The reasoning behind not creating apphints silently
		   is that they need an ACL to be setup.  Without that,
		   the storage space of Reservoir would be a cloaked
		   storage service for anyone who can think up a name.
		"""
		#
		# Pickup current colluuid, quietly leave if already set
		colluuid = self.colluuid
		if colluuid is not None:
			return
		#
		# Use the apphint from the Home Index
		if colluuid is None and self.apphint is not None:
			colluuid = self._index_lookup (' ' + self.apphint)
		#
		# Clone the apphint from the domain's Home Index
		if colluuid is None and self.apphint is not None and self.user is not None:
			domidx = Index (self.domain, apphint=self.apphint)
			template = domidx._index_lookup (' ' + self.apphint)
			if template is None:
				template = { }
				colluuid = None
			else:
				# Check ACL -- maybe reject the current visitor
				template.check_acl (template.get_colluuid ())
				add_collection (self, template ['cn'][0])
			def make_it_mine (ac):
				if ac == '@' + self.domain:
					return self.user + ac
				else:
					return ac
			for (k,vs) in template.items ():
				if k in [ 'resourceInstance', 'resins', 'reservoirRef']:
					# Never clone these values
					continue
				if k == 'acl':
					self [k] = map (make_it_mine, vs)
				#TODO# Possibly more specialisation
				pass
		#
		# Use the default/unnamed apphint in the Home Index
		if colluuid is None:
			colluuid = self._index_lookup ('')
		#
		# Report if no apphint source found
		if colluuid is None:
			raise Exception ('Could not resolve apphint; stuck in Home Index')
		#
		# Follow the apphint, but not before checking the ACL
		self.set_colluuid (colluuid)

	def list_index (self):
		"""List the names of the current Index.  Also cache
		   the translation to Collection UUID, but do not
		   return it; index_name() can be used to access each
		   name instead.
		"""
		if self.curidx is None:
			self._index_load ()
		return [ entry [37:]
		           for entry in self.curidx
		           if len (entry) >= 37 ]

	def list_apphints (self):
		"""List the apphints while on a Home Index.
		"""
		assert self.colluuid is None, 'Call list_apphints() from a Home Index'
		return self.list_index ()

	def __getitem__ (self, k):
		if self.curidx is None:
			self._index_load ()
		k = ' ' + k if k is not None else ''
		for entry in self.curidx:
			if entry [36:] == k:
				return entry [:36]
		raise KeyError ('Name "%s" not found in %s' % (k,str(self)))

	def __setitem__ (self, k, v):
		assert re_uuid.match (v)
		if self.curidx is None:
			self._index_load ()
		newidx = '%s %s' % (v,k)
		dn1 = str (self.dn)
		dap.modify_s (dn1, [(MOD_ADD,'reservoirRef',s2b(newidx))])
		self.curidx.append (newidx)
		mqtt_notify_ref (self, k, v)

	def __delitem__ (self, k):
		v = self.__getitem__ (k)
		oldidx = '%s %s' % (v,k) if k is not None else v
		dap.modify_s (str (self), [(MOD_DELETE,'reservoirRef',s2b(oldidx))])
		self.curidx.remove (oldidx)
		mqtt_notify_ref (self, k, None)

	def __contains__ (self, k):
		return k in self.keys ()

	def items (self):
		if self.curidx is None:
			self._index_load ()
		return [ (entry [37:] if len(entry)>36 else None ,entry[:36]) for entry in self.curidx ]

	def keys (self):
		return [ k for (k,_) in self.items () ]

	def values (self):
		return [ v for (_,v) in self.items () ]

	def index_name (self, name, defaultOK=False):
		"""Find the given name in the index.  If it is absent
		   and defaultOK is set, the default/unnamed index
		   entry is used instead.  When name is None, this is
		   in fact the only option (but not assumed by the flag).
		   #TODO# defaultOK is standard for a home index (no colluuid)
		"""
		assert defaultOK or name is not None, 'Index lookups require either a name or welcoming the default/unnamed entry'
		if self.colluuid is None:
			self.use_apphint ()
		if name is not None:
			colluuid = self._index_lookup (' ' + name)
		if colluuid is None and defaultOK:
			colluuid = self._index_lookup ('')
		if colluuid is None:
			if defaultOK:
				name = name +' or default/unnamed entry'
			raise KeyError ('Name "%s" not found in %s' % (name,str(self)))
		self.check_acl (colluuid)
		self.colluuid = colluuid
		self.curidx = None

	def index_path (self, *path):
		"""Traverse a sequence of path names, none of which may be
		   None and none of which can use the default/unnamed entry.
		   Note that paths break through the ACL of intermediate
		   steps.  It is only the last index whose ACL matters.
		"""
		assert not name in path, 'Index paths cannot contain None'
		if self.colluuid is None:
			self.use_apphint ()
		cur_colluuid = self.colluuid
		for step in path:
			cur_colluuid = self._index_lookup (' ' + step, cur_colluuid)
			if cur_colluuid is None:
				raise KeyError ('Path element %s not found in Index' % step)
		self.check_acl (cur_colluuid)
		self.colluuid = cur_colluuid
		self.curidx = None

	def index_reverse (self):
		"""Return a (reverse) index with references to this object.
		   These are the Index objects under the same domain from
		   domain, user and Collection objects, with a reservoirRef
		   attribute pointing to this object.
		"""
		if self.colluuid is None:
			raise Exception ('Reverse indexing is not applicable to Home Indexes')
		assert len (self.colluuid) == 36
		dn1 = self.dn_domain ()
		fl1 = '(&(objectClass=reservoirIndex)(reservoirRef=%s*))' % self.colluuid
		al1 = ['reservoirRef']
		qr1 =       dap.search_s (dn1, SCOPE_ONELEVEL, filterstr=fl1, attrlist=al1)
		qr1.extend (dap.search_s (dn1, SCOPE_BASE,     filterstr=fl1, attrlist=al1))
		return [ Index.from_dn (dn2) for (dn2,_) in qr1 ]

	def resource_dn (self, resuuid):
		"""Given a resource UUID, return the DN of its object.
		"""
		if self.colluuid is None:
			self.use_apphint ()
		return 'resource=%s,resins=%s,associatedDomain=%s,%s' % (resuuid,self.colluuid,self.domain,reservoir_base)

	def load_resource (self, resuuid):
		"""Load a single resource from LDAP and return it as a
		   Resource object.  Use this when you know its UUID, but
		   for lists the load_all_resources() method may be more
		   efficient.
		"""
		if self.colluuid is None:
			self.use_apphint ()
		me = copy.copy (self)
		return Resource (me, resuuid)

	def list_resources (self):
		"""Return a possibly-empty list of resource UUIDs under the
		   current index.  This is not possible in a home node of a
		   domain or user, because an apphint must first be given.
		   The ACL will be checked live, to avoid lingering objects
		   that are used after their valid lives have expired.
		"""
		if self.colluuid is None:
			self.use_apphint ()
		self.check_acl (self.colluuid)
		try:
			dn1 = str (self)
			fl1 = '(objectClass=reservoirResource)'
			al1 = ['resource']
			#DEBUG# print ('searching', dn1, fl1, al1)
			qr1 = dap.search_s (dn1, SCOPE_ONELEVEL, filterstr=fl1, attrlist=al1)
			#DEBUG# print ('Query response:', qr1)
			retval = [ ]
			for (dn,at) in qr1:
				if 'resource' in at:
					retval += b2s (at ['resource'])
			return retval
		except NO_SUCH_OBJECT:
			return []

	def load_all_resources (self):
		"""Return a possibly-empty list of resources in the current
		   index.  This is not possible in a home node of a domain or
		   user, because from there an apphint must first be given.
		   The ACL will be checked live, to avoid lingering objects
		   that are used after their valid lives have expired.
		   TODO: Can we skip the ACL completely before?
		"""
		me = copy.copy (self)
		return [ Resource (me, resuuid)
		         for resuuid in self.list_resources () ]


def add_domain (domain, orgname=None):
	"""Introduce a domain to Reservoir and possibly an organisation name
	   along with it.  Create a default/unnamed index referencing a new
	   Collection object.  Return a new Index set to the domain's home.
	   #TODO# Treat current user as admin, and set the ACL accordingly.
	"""
	newdir = '%s/%s' % (reservoir_home,domain)
	os.mkdir (newdir)
	home_colluuid = random_uuid ()
	dn1 = 'associatedDomain=%s,%s' % (domain,reservoir_base)
	at1 = s2b ([
		('objectClass', [b'organization',b'domainRelatedObject',b'reservoirIndex',b'aclObject',b'labeledURIObject']),
		('o', orgname or domain),
		('associatedDomain', domain),
		('reservoirRef', [home_colluuid]),
		('acl', '%%w @%s' % domain),
	])
	try:
		meta = dap.add_s (dn1,at1)
	except ALREADY_EXISTS:
		raise FileExistsError ('Domain %s was already introduced to the Reservoir' % domain)
	dn2 = 'resins=%s,%s' % (home_colluuid,dn1)
	at2 = s2b([
		('objectClass', [b'reservoirCollection',b'resourceInstanceObject',b'aclObject',b'reservoirIndex']),
		('rescls', reservoir_uuid),
		('resins', home_colluuid),
		('cn', 'Home Collection of %s' % domain),
		('acl', '555 %%r @%s %%w admin@%s' % (domain,domain)),
	])
	#DEBUG# print ('adding', dn2)
	dap.add_s (dn2, at2)
	newdir = '%s/%s/%s' % (reservoir_home,domain,home_colluuid)
	os.mkdir (newdir)
	idx = Index (domain)
	mqtt_notify_ref (idx, None, home_colluuid)
	mqtt_notify_uribases (domain, [])
	return idx


def del_domain (domain, recursive=False):
	"""Delete a domain object.  This will raise an exception if not all
	   objects underneath have been removed first.  Use recursive=True
	   to also remove such underlying objects (users, Collections and
	   Resources).
	"""
	domidx = get_domain (domain)
	dn1 = domidx.dn_domain ()
	dropidx = [ ]
	if recursive:
		fl1 = '(objectClass=reservoirIndex)'
		sc1 = SCOPE_ONELEVEL
		try:
			qr1 = dap.search_s (dn1, sc1, filterstr=fl1, attrlist=[])
			for (dn2,_) in qr1:
				assert dn1 != dn2, 'LDAP searches ONELEVEL should not include BASE'
				dropidx.append (Index.from_dn (dn2))
		except ldap.NO_SUCH_OBJECT:
			pass
	dropidx.append (domidx)
	# First, remove all reservoirRef (and report emptyness over MQTT)
	for idx in dropidx:
		for k in idx.keys ():
			del idx [k]
	# Second, actually drop all Collections with their Resources
	for idx in dropidx:
		del_collection (idx, unlink_refs=False, del_resources=True)
	# Third, reset all URI bases
	mqtt_notify_uribases (domain, None)
	# Fourth, remove all traces from the file system
	print ('Removing tree %s/%s' % (reservoir_home,domain))
	shutil.rmtree ('%s/%s' % (reservoir_home,domain), ignore_errors=True)


def get_domain (domain, apphint=None):
	"""Retrieve an Index object that is home to the given domain name,
	   which already exist in the Reservoir.
	"""
	return Index (domain, apphint=apphint)


def list_domains ():
	"""Retrieve a list of domain names that are registered with
	   the Reservoir.
	"""
	retval = [ ]
	try:
		dn1 = reservoir_base
		fl1 = '(objectClass=domainRelatedObject)'
		al1 = ['associatedDomain']
		#DEBUG# print ('searching', dn1, fl1, al1)
		qr1 = dap.search_s (dn1, SCOPE_ONELEVEL, filterstr=fl1, attrlist=al1)
		#DEBUG# print ('Query response:', qr1)
		# Note that we always set a single associatedDomain
		# but domainRelatedObject does not set it as SINGLE-VALUE
		for (dn,at) in qr1:
			if 'associatedDomain' in at:
				retval += b2s (at ['associatedDomain'])
	except NO_SUCH_OBJECT:
		pass
	return retval


def add_domain_user (domain, user):
	"""Add a user node under an already created domain name.  Create a
	   default/unnamed index referencing a new Collection object.
	   Return a new Index set to the user's home.
	"""
	home_colluuid = random_uuid ()
	dn1 = 'uid=%s,associatedDomain=%s,%s' % (user,domain,reservoir_base)
	at1 = s2b ([
		('objectClass', [b'account',b'uidObject',b'domainRelatedObject',b'reservoirIndex',b'aclObject',b'labeledURIObject']),
		('uid', user),
		('associatedDomain', domain),
		('reservoirRef', [home_colluuid]),
		('acl', '%%w %s@%s' % (user,domain)),
	])
	try:
		meta = dap.add_s (dn1,at1)
	except ALREADY_EXISTS:
		raise FileExistsError ('User %s of domain %s was already introduced to the Reservoir' % (user,domain))
	dn2 = 'resins=%s,%s' % (home_colluuid,dn1)
	at2 = s2b ([
		('objectClass', [b'reservoirCollection',b'reservoirIndex',b'resourceInstanceObject',b'accessControlledObject']),
		('rescls', reservoir_uuid),
		('resins', home_colluuid),
		('cn', 'Home Collection of %s@%s' % (user,domain)),
		('acl', '%%w %s@%s' % (user,domain)),
	])
	#DEBUG# print ('adding', dn2)
	dap.add_s (dn2, at2)
	newdir = '%s/%s/%s' % (reservoir_home,domain,home_colluuid)
	os.mkdir (newdir)
	idx = Index (domain, user)
	mqtt_notify_ref (idx, None, home_colluuid)
	return idx


def del_domain_user (domain, user):
	"""Delete a user node under a domain.  This assumes that all the
	   referenced Collections (Apphints) have already been removed.
	"""
	idx = Index (domain, user)
	for clx in idx.values ():
		# Erase apphint Collections (auto-created, and mine)
		del_collection (clx)
	dap.delete_s (dn1)
	mqtt_notify (idx, MQTT_DEL)


def get_domain_user (domain, user, apphint=None):
	"""Return an Index object that is home to the given user@domain,
	   which must already exist in the Reservoir.
	"""
	return Index (domain, user, apphint=apphint)


def list_domain_users (domain):
	"""Retrieve a list of users that are registered under a domain with
	   the Reservoir.  The domain is either an Index object or a string.
	"""
	retval = [ ]
	try:
		if isinstance (domain,Index):
			dn1 = domain.dn_domain ()
		else:
			dn1 = 'associatedDomain=%s,%s' % (domain,reservoir_base)
		fl1 = '(objectClass=domainRelatedObject)'
		al1 = ['uid']
		#DEBUG# print ('searching', dn1, fl1, al1)
		qr1 = dap.search_s (dn1, SCOPE_ONELEVEL, filterstr=fl1, attrlist=al1)
		#DEBUG# print ('Query response:', qr1)
		# Note that we always set a single associatedDomain
		# but domainRelatedObject does not set it as SINGLE-VALUE
		for (dn,at) in qr1:
			if 'uid' in at:
				retval += at ['uid']
	except NO_SUCH_OBJECT:
		pass
	return retval


def add_collection (index, collname):
	"""Add a new Resource Collection under the domain for the given
	   Index.  Move this Index object to the new Collection.
	"""
	domain = index.get_domain ()
	colluuid = random_uuid ()
	newdir = '%s/%s/%s' % (reservoir_home,domain,colluuid)
	os.mkdir (newdir)
	dn1 = 'resins=%s,associatedDomain=%s,%s' % (colluuid,domain,reservoir_base)
	at1 = s2b ([
		('objectClass', [b'reservoirCollection',b'reservoirIndex',b'resourceInstanceObject',b'aclObject']),
		('rescls', reservoir_uuid),
		('resins', colluuid),
		('cn', collname or 'Anonymous Resource Collection'),
		('acl', '%%w @%s' % domain),
	])
	#DEBUG# print ('adding', dn1)
	dap.add_s (dn1, at1)
	dn2 = str (index)
	ref = [ s2b ('%s %s' % (colluuid,collname)) ]
	at2 = [ (MOD_ADD,'reservoirRef',ref) ]
	dap.modify_s (dn2, at2)
	print ('mqtt_notify_ref (%r,%r,%r)' % (index,collname,colluuid) )
	mqtt_notify_ref (index, collname, colluuid)
	index.set_colluuid (colluuid)


def del_collection (victim, unlink_refs=True, del_resources=True):
	"""Delete the Collection whose Index instance is given.  By default,
	   this includes the removal of underlying resources (due to the usually
	   set flag del_resources) and reservoirRef attributes pointing here
	   (due to unlink_refs).  When these flags are overridden to False, they
	   can raise exceptions when things-that-need-us are still there.  It is
	   never permitted to remove a Collection that has references to others;
	   in lieu of a del_recursive option you shall have to do this manually.
	"""
	domain = victim.get_domain ()
	colluuid = victim.get_colluuid ()
	subclx = victim.list_index ()
	# Distinguish Home Index from plain Collection
	if colluuid is None:
		# Home Index, neither Resource objects nor Reverse Index
		resobj = []
		revidx = []
	else:
		# Collection, find Resource objects and Reverse Index references
		if subclx != []:  #TODO:# and not del_recursive:
			raise Exception ('Remove forward references first, or permit TODO:recursive removal')
		resobj = victim.load_all_resources ()
		if resobj != [] and not del_resources:
			raise Exception ('Remove collected resources first, or permit their automatic removal')
		revidx = victim.index_reverse ()
		if revidx != [] and not unlink_refs:
			raise Exception ('Remove reverse references first, or permit their automatic removal')
	# Clean references to the victim from the reverse Index
	for refidx in revidx:
		for (k,v) in refidx.items ():
			print ('DEBUG: About to delete %r -> %r' % (k,v))
			if v == colluuid:
				del refidx [k]
	# Remove sub-Collections recursively
	for clx in subclx:
		del_collection (clx, unlink_refs=unlink_refs, del_resources=del_resources)  # del_recursive=del_recursive
	# Remove Resources for this Collection
	for resource in resobj:
		del_resource (resource)
	# Remove from LDAP and the file system
	dn1 = victim.dn_cursor ()
	dap.delete_s (dn1)
	if colluuid is not None:
		print ('Removing tree %s/%s/%s' % (reservoir_home,domain,colluuid))
		shutil.rmtree ('%s/%s/%s' % (reservoir_home,domain,colluuid), ignore_errors=True)


def get_collection (index, colluuid):
	"""Find the Resource Collection with the given UUID.  Move the
	   given Index object to that Collection.
	"""
	index.set_colluuid (colluuid)


def dn_collection (resdn):
	"""Return a Collection object for a given DN, or None.
	"""
	m = re_collection_dn.match (resdn)
	if m is None:
		return None
	(colluuid,domain) = m.groups ()
	idx = get_domain (domain)
	idx.set_colluuid (colluuid)
	return idx


def add_resource (index, **meta):
	"""Add a new Resource under the given Index (which must not be
	   a home index, so use the apphint before doing this).  Store the
	   metadata as attributes.  Add auxiliary object classes if needed,
	   but at least include the 'reservoirResource' objectClass.  Also
	   provide at least a mediaType and a uniqueIdentifier attribute.
	   The mediaType is also known as a MIME-type, the uniqueIdentifier
	   must be unique within the Collection, so it works like a file name.
	   The underlying file will be created empty.  The created Resource
	   is returned as an object, and it may be used to open() the file.
	"""
	coll_dn = index.dn_cursor ()
	colluuid = index.get_colluuid ()
	domain = index.get_domain ()
	resuuid = random_uuid ()
	newfile = '%s/%s/%s/%s' % (reservoir_home,domain,colluuid,resuuid)
	os.close (os.open (newfile, os.O_CREAT | os.O_WRONLY))
	dn1 = 'resource=%s,%s' % (resuuid,coll_dn)
	assert 'reservoirResource' in meta.get ('objectClass',[]), 'Added resources must declare objectClass:reservoirResource (and may add auxiliary classes)'
	assert 'mediaType' in meta, 'Media type must be provided'
	assert 'uniqueIdentifier' in meta, 'uniqueIdentifier must be provided'
	at1 = [ (k,s2b(v)) for (k,v) in meta.items () ]
	at1.append ( ('resource',s2b(resuuid)) )
	#DEBUG# print ('adding', dn1)
	dap.add_s (dn1, at1)
	res = index.load_resource (resuuid)
	mqtt_notify (res, MQTT_ADD)
	return res


def del_resource (victim):
	"""Delete a Resource whose object is presented.
	"""
	domain = victim.get_domain ()
	colluuid = victim.get_colluuid ()
	resuuid = victim.get_resuuid ()
	dn1 = victim.dn_resource ()
	dap.delete_s (dn1)
	os.remove ('%s/%s/%s/%s' % (reservoir_home,domain,colluuid,resuuid))
	mqtt_notify (victim, MQTT_DEL)


def dn_resource (resdn):
	"""Return a Resource object for a given DN, or None.
	"""
	m = re_resource_dn.match (resdn)
	if m is None:
		return None
	(resuuid,colluuid,domain) = m.groups ()
	idx = get_domain (domain)
	idx.set_colluuid (colluuid)
	rsc = get_resource (idx, resuuid)
	return rsc


def clone_resource (resource, index, uniqueIdentifier):
	"""Add a new Resource under the given Index (which must not be
	   a home index, so use the apphint before doing this).  Store the
	   same metadata, but construct a new resource UUID and replace the
	   uniqueIdentifier; this allows the new Resource to be created
	   under the same Index, or to move it there in the future.  The
	   created Resource is returned as an object, and it may be used
	   to open() the file.  This function does not copy objects across
	   domains, for security reasons.
	"""
	if resource.get_domain () != index.get_domain ():
		raise Exception ('Cross-domain cloning of Resource is not considered secure')
	meta = { k:( set([uniqueIdentifier]) if k=='uniqueIdentifier' else v )
	         for (k,v) in resource.items ()
	         if k not in ['resource'] }
	print ('DEBUG: meta %r' % (meta,))
	newres = add_resource (index, **meta)
	if newres is not None:
		with resource      .open (reading=True, writing=False, binary=True) as oldfh:
			with newres.open (reading=False, writing=True, binary=True) as newfh:
				newfh.write (oldfh.read ())
		# Since the documentHash was already copied, we need not commit()
		newres.rollback ()
	return newres


def move_resource (resource, index, uniqueIdentifier=None):
	"""Move the given Resource into the given Index.  If the same
	   Index is used, this call is quietly ignored.  This function
	   does not move Resources across domains, for security reasons.
	   When a uniqueIdentifier is provided, the Resource object will
	   have this new information set for it.  This does mean that a
	   commit() is needed to save the change to LDAP.  This Index
	   must not be a home index, so use the apphint before doing this).
	"""
	domain = resource.get_domain ()
	if domain != index.get_domain ():
		raise Exception ('Cross-domain moving of Resource is not considered secure')
	if uniqueIdentifier is not None:
		#TODO# Might check if the name already occurs under index
		resource ['uniqueIdentifier'] = set ([uniqueIdentifier])
	olddn = resource.dn_resource ()
	resuuid = resource.get_resuuid ()
	newresrdn = 'resource=%s' % resuuid
	newindexdn = index.dn_cursor ()
	if resource.get_colluuid () == index.get_colluuid ():
		return
	oldfn = '%s/%s/%s/%s' % (reservoir_home, domain, resource.get_colluuid (), resuuid)
	newfn = '%s/%s/%s/%s' % (reservoir_home, domain, index   .get_colluuid (), resuuid)
	dap.rename_s (olddn, newresrdn, newindexdn, delold=0)
	os.rename (oldfn, newfn)
	mqtt_notify (resource, MQTT_DEL)
	resource.set_colluuid (index.get_colluuid ())
	mqtt_notify (resource, MQTT_ADD)


def get_resource (index, resuuid):
	"""Load a resource as identified by UUID under the given Index.
	"""
	return index.load_resource (resuuid)


def search_resources (index, ldap_filter):
	"""Search for resources under the given Index (which must not be
	   a home index, so use the apphint first).  Use the given LDAP
	   filter expression to select objects.  The return value is a
	   (possibly empty) dict with Resource instances indexed by their
	   UUID.  The Resource instances have a cache of attributes, but
	   there is no guarantee that nobody else has another cache.
	"""
	dn1 = str (index)
	assert dn1 [:7] == 'resins=', 'No resources under a home Index, so searching is unsupported'
	fl1 = '(&(objectClass=reservoirResource)%s)' % ldap_filter
	#DEBUG# print ('searching', dn1, fl1, al1)
	qr1 = dap.search_s (dn1, SCOPE_ONELEVEL, filterstr=fl1)
	retval = { }
	index2 = copy.copy (index)
	for (dn,at) in qr1:
		if 'resource' in at:
			atres = at ['resource']
			if len (atres) != 1:
				continue
			resuuid = b2s (atres [0])
			retval [resuuid] = Resource (index2, resuuid, at)
	return retval


def uri_canonical (domain, cursor=None, user=None, apphint=None, path=[], domain_relative=False):
	"""Given a cursor and a user (which may be None), work through
	   the path and canonicalise it.  The correct result is always
	   a Collection (in the form of an Index) and possibly also a
	   Resource.  In addition, a URI without scheme is constructed,
	   in the form //domain/colluuid/[resuuid] called canonical.
	   If you specify the domain_relative flag, the form will rely
	   on the context to provide the domain, which is easier for
	   some external uses.  The form /colluuid/[resuuid] is returned.
	   This function returns the tuple (uri,colidx,resobj|None) on
	   success, or raises an Exception or Error on failure.
	   
	     * Normally, the search starts at an initial Collection.
	       This is the home node for the domain or, when a user
	       is provided, for that user under the domain.  When an
	       apphint is provided it will be followed next, and if
	       it is not provided the default/unnamed entry in the
	       home node is used to arrive at the initial Collection.
	   
	     * When a cursor is provided, it must be an Index that
	       will continue as the initial Collection.  The other
	       mechanisms for deriving it are ignored, because it
	       is assumed that any such information is already
	       taken into account in the cursor.  Note that there
	       may be inconsistencies, leading to unexpected results,
	       if the other parameters differ from what was used to
	       setup the cursor.  This may not be detected here.
	       The cursor will be taken along the, ehm, path.
	   
	     * Normally, the path contains a sequence of strings that
	       do not look like a UUID.  They are steps taken from an
	       initial Collection to the final Collection.  When a
	       path string is not found, this function fails.
	   
	     * Special paths may start with a UUID form.  This would
	       override the initial Collection, and so there is no
	       use for the user and apphint anymore; the domain is a
	       security concern and must always be supplied.
	   
	     * Special paths may end with a UUID form that is not
	       also a starting UUID.  This indicates a Resource,
	       and this would be provided in the return value
	       instead of the None value if no Resource was set.
	   
	     * Other than first and last path element, the UUID form
	       must not occur for path elements.
	   
	     * This routine being about canonicalisation, it does not
	       use attributes to locate a set of resources.  This may
	       well be desired, perhaps from URI parameters.  When
	       the canonical form includes no Resource yet, such a
	       search can be performed on the final Collection using
	       the search_resources function.
	   
 	     * This routine does not prepend a scheme and colon, such as
	       "https:"; this is usually interpreted as a reference to
	       a context that provides this information, and we use it
	       to avoid being overly restrictive in the canonical form.
	       URIs that do start with a scheme can be reduced to the
	       canonical form by stripping anything before the "//".
	   
	     * This routine does not append any attributes that could
	       help another host to load or store a Resource in their
	       systems.  If the resource returned is None, this may
	       be valuable to some purposes, and they may then be
	       added using "?...&..." query parameters; note that the
	       canonical form never contains query parameters.  These
	       extensions make the URI deviate from the canonical
	       representation, but is often useful and the canonical
	       can be included as the part before the first "?".
	"""
	if len (path) >= 1 and re_uuid.match (path [0]):
		# Path starts with UUID of Initial Collection
		clx = get_domain (domain)
		get_collection (clx, path [0].lower ())
		path = path [1:]
	elif cursor is not None:
		clx = cursor
	else:
		if user is None:
			# Just a domain
			clx = get_domain (domain, apphint=apphint)
		else:
			# Domain with User
			clx = get_domain_user (domain, user, apphint=apphint)
	clx.use_apphint ()
	# Pickup a potential Resource UUID at the end of the path
	res = None
	resuuid = None
	if len (path) >= 1 and re_uuid.match (path [-1]):
		# Path ends with UUID of Resource
		resuuid = path [-1].lower ()
		path = path [:-1]
	# Process the non-UUID path elements
	for step in path:
		assert not re_uuid.match (step), 'Inner path elements must not have a UUID form'
		assert res is None, 'Found a Resource by uniqueIdentifier, rejecting further Collections'
		try:
			clx.index_name (step)
		except KeyError as ke:
			if resuuid is not None:
				raise
			step = urllib.parse.quote (step)
			ress = search_resources (clx, '(uniqueIdentifier=%s)' % step)
			if len (ress) != 1:
				raise ke
			[(resuuid,res)] = ress.items ()
	# Now find the Resource|None to output
	if res is None and resuuid is not None:
		res = clx.load_resource (resuuid)
	# Form the URI
	colluuid = clx.get_colluuid ()
	if domain_relative:
		uri =     '/%s/%s' % (       colluuid,resuuid or '')
	else:
		uri = '//%s/%s/%s' % (domain,colluuid,resuuid or '')
	# Return the canonical triple
	return (uri,clx,res)


def uri_canonical_open (canonuri, domain=None, domain_relative=False):
	"""Open a canonical URI, possibly relative to a given domain.
	   The recognised forms are strictly validated to be one of
	   //domain/colluuid/resuuid or //domain/colluuid/ or
	   /colluuid/resuuid or /colluuid/ and an exception is raised
	   otherwise.  The domain_relative flag imposes extra errors
	   when the canonuri starts with the //domain prefix.
	   On positive result, the return value is (collection,[resource])
	   where the Index is for the colluuid and the resource is
	   either for the resuuid or, if not provided, set to None.
	"""
	if canonuri [:1] != '/':
		raise Exception ('Canonical URI must start with a slash')
	if canonuri [:2] == '//':
		# Absolute form, ignore/override domain parameter
		if domain_relative:
			raise Exception ('Relative URI was enforced, but an absolute form provided')
		canonuri = canonuri [2:]
		slash = canonuri.find ('/')
		if slash == -1:
			raise Exception ('Canonical URI requires /colluuid/ after //domain)')
		domain = canonuri [:slash]
		canonuri = canonuri [slash:]
	# Relative form
	if domain is None:
		raise Exception ('Relative form of Canonical URI needs a domain ')
	elif False:
		#TODO# Possibly match domain syntax
		pass
	if canonuri [:1] != '/':
		raise Exception ('Canonical URI requires a / before the relative part')
	canonuri = canonuri [1:]
	slash  = canonuri. find ('/')
	slashr = canonuri.rfind ('/')
	if slash == -1 or slash != slashr:
		raise Exception ('Precisely one slash required in either colluuid/ or colluuid/resuuid')
	colluuid = canonuri [:slash]
	if not re_uuid.match (colluuid):
		raise Exception ('The format of /colluuid/ is not a UUID')
	elif False:
		#TODO# Might require lowercase: colluuid != colluuid.lower ()
		pass
	resuuid = canonuri [slash+1:]
	if resuuid == '':
		resuuid = None
	elif not re_uuid.match (resuuid):
		raise Exception ('The format of /resuuid is not a UUID')
	elif False:
		#TODO# Might require lowercase: resuuid != resuuid.lower ()
		pass
	# Open the Collection through its Index
	clx = Index (domain)
	clx.set_colluuid (colluuid)
	# Open the Resource if one was given
	res = None
	if resuuid is not None:
		res = clx.load_resource (resuuid)
	return (clx,res)


def uri_basetofull (base, domain, colluuid, resuuid=None):
	"""Extend a base URI with the components of the canonical URI
	   to form a full URI.
	   
	   Inputs to this process are domain, colluuid and optional
	   resuuid, for the canonical forms //domain/colluuid/ and
	   //domain/colluuid/resuuid that can mingle with the base.
	   
	   The base URI provides a scheme:, noptional prefix to the
	   domain to turn it into a host, optional port to deviate
	   from the scheme's default, optional initial path to
	   prefix to the /colluuid/ or /colluuid/resuuid endings,
	   and optional ?query parameters.  When a #fragment is
	   present it will also be passed into the full URI, with
	   or without ?query parameters.
	   
	   The reverse procedure is uri_fromfull().
	"""
	(schema,host,path,params,query,fragment) = urllib.parse.urlparse (base)
	if ':' in host:
		(host,port) = host.split (':')
	else:
		port = None
	if host == '':
		host = domain
	else:
		host = host + '.' + domain
	if port is not None:
		host = host + ':' + port
	if path [-1:] == '/':
		path = path + colluuid + '/'
	else:
		path = path + '/' + colluuid + '/'
	if resuuid is not None:
		path = path + resuuid
	return urllib.parse.urlunparse ( (schema,host,path,params,query,fragment) )


def uri_fromfull (full, base):
	"""Take a full URI apart with hints from its base, and return
	   the components of the canonical URI: domain, colluuid and
	   either resuuid or None.
	   
	   To obtain the string form of a canonical URI, apply the
	   output of this routine to formatting string '//%s/%s/%s'
	   after extending resuuid with "or ''" so it exists.
	   
	   This procedure reverses uri_basetofull().  There may be slight
	   modifications in situations where the original base URI held
	   unused delimiters, such as a '?' without any parameters.
	"""
	(schema0,host0,path0,params0,query0,fragment0) = urllib.parse.urlparse (base)
	(schema1,host1,path1,params1,query1,fragment1) = urllib.parse.urlparse (full)
	if schema0 != schema1:
		raise Exception ('Schema mismatch: %s != %s' % (schema0,schema1))
	if (':' in host0) != (':' in host1):
		raise Exception ('Port inconsistency: %s != %s' % (host0,host1))
	if ':' in host0:
		(host0,port0) = host0.split (':')
		(host1,port1) = host1.split (':')
		if port0 != port1:
			raise Exception ('Port mismatch: %s != %s' % (port0,port1))
	if len (host0) > 0 and host0 [-1] != '.':
		host0 = host0 + '.'
	if not host1.startswith (host0):
		raise Exception ('Hostname mismatch: %s is not a host under %s' % (host1,host0))
	domain = host1 [len(host0):]
	if not path0.endswith ('/'):
		path0 = path0 + '/'
	if not path1.startswith (path0):
		raise Exception ('Path redirected: %s does not extend %s' % (path1,path0))
	path1 = path1 [len (path0):]
	colluuid = path1 [:36]
	if not re_uuid.match (colluuid):
		raise Exception ('Path extension %s does not start with UUID' % path1)
	resuuid = None
	if len (path1) > 36:
		if path1 [36] != '/':
			raise Exception ('Improper separation between colluuid and resuuid: %s' % path1)
		path1 = path1 [37:]
		if path1 != '':
			if not re_uuid.match (path1):
				raise Exception ('Expected resource UUID but found %s' % path1)
			resuuid = path1
	return (domain,colluuid,resuuid)


def add_uribase (domain, baseuri, comment=None):
	"""Given a domain Index, add a base URI and optional end-user
	   comment.  Adding a comment classifies the base URI as an
	   end-user services; without it it merely serves as a potential
	   backend to end-user services.  The domain Index must currently
	   be in the domain Home Index (and so it cannot have a user).
	"""
	domhom = domain.dn_domain ()
	domcrs = domain.dn_cursor ()
	assert domhom == domcrs, 'The domain is not in the Home Index but %s' % domcrs
	if comment is not None:
		baseuri = '%s %s' % (baseuri,comment)
	dn1 = domhom
	at1 = [ (MOD_ADD,'labeledURI',s2b(baseuri)) ]
	dap.modify_s (dn1, at1)
	mqtt_notify_uribases (domain.get_domain (), list_uribases (domain))


def del_uribase (domain, baseuri):
	"""Given a domain Index, remove the base URI, regardless of
	   comments it might have.  Returns the number flushed, which
	   may be 0, 1 or more.  A KeyError is raised if no labeledURI
	   is in the current Index.  An AssertionError is raised if the
	   domain Index is not a domain Home Index.
	"""
	domhom = domain.dn_domain ()
	domcrs = domain.dn_cursor ()
	assert domhom == domcrs, 'The domain is not in the Home Index but %s' % domcrs
	dn1 = domhom
	al1 = [ 'labeledURI' ]
	qr1 = dap.search_s (dn1, SCOPE_BASE, attrlist=al1)
	mods = [ ]
	kept = [ ]
	for (luri,comment) in list_uribases (domain):
		if luri != baseuri:
			kept.append ( (luri,comment) )
			continue
		if comment is not None:
			bluri = s2b ('%s %s' % (luri,comment))
		else:
			bluri = s2b ('%s'    %  luri         )
		mods.append( (MOD_DELETE,'labeledURI',bluri) )
	if len (mods) > 0:
		qr2 = dap.modify_s (dn1, mods)
		mqtt_notify_uribases (domain.get_domain (), kept)
	return len (mods)


def list_uribases (domain, backend=True, enduser=True):
	"""List base URIs for the given domain Index and return a list
	   of tuples (baseuri,[comment]) where the [comment] is None
	   in case no comment was posted along the baseURI, indicative
	   of a backend baseURI rather than an enduser baseURI.  An empty
	   list is a possible and valid return value.  The domain Index
	   must be in the domain Home Index.
	   
	   Note that a base URI is not useful on its own; first call
	   uri_basetofull() or equivalent code in your local library
	   to set it up with specific (domain,colluuid,[resuuid]) data.
	   The essential use of base URIs is to be able to construct
	   such strings in an automated fashion, not to serve clicking
	   zealots :)
	"""
	domhom = domain.dn_domain ()
	domcrs = domain.dn_cursor ()
	assert domhom == domcrs, 'The domain is not in the Home Index but %s' % domcrs
	dn1 = domhom
	al1 = [ 'labeledURI' ]
	qr1 = dap.search_s (dn1, SCOPE_BASE, attrlist=al1)
	retval = [ ]
	for (dn2,at2) in qr1:
		assert dn2 == dn1, 'LDAP base search deviated from object'
		for luri in at2.get ('labeledURI', []):
			sluri = b2s (luri)
			if ' ' in sluri:
				(sluri,comment) = sluri.split (' ', 1)
				use_it = enduser
			else:
				comment = None
				use_it = backend
			if use_it:
				retval.append ( (sluri,comment) )
	return retval

