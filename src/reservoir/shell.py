#!/usr/bin/env python
#
# arpa2.reservoir.shell -- Administration utility for Reservoir
#
# This commandline TODO:uses the Reservoir API to update LDAP.
# It is useful as an administrator's tool which, based on
# the ACL mechanism, could be opened up to the public!
#
# The ARPA2 shell mechanism includes a few facilities:
#  - arpa2shell is a meta-shell that can start this one
#  - arpa2shell adds commands to return to other shells
#  - arpa2amqp.d acccesses this shell for JSON commands
#
# From: Rick van Rein <rick@openfortress.nl>


import os
import sys
import uuid
import re
import hashlib

# Shell support: base classes, meta connections, cmdline/syntax parsing
from arpa2.shell import cmdshell, cmdparser

# The Reservoir API
from arpa2 import reservoir



# Add a domain to LDAP; Riak KV treats this level as implicitly created
#
def cmd_domain_add (domain, orgname=None):
	reservoir.add_domain (domain, orgname)


# Delete a domain from LDAP; Riak KV treats this level as implicitly created
#
def cmd_domain_del (tree, *domains):
	for dom in domains:
		try:
			print ('Calling API to delete', dom)
			reservoir.del_domain (dom, recursive=tree)
		except Exception as e:
			print ('Failed:', str (e))
			raise
			sys.exit (1)

def cmd_domain_list ():
	for domain in reservoir.list_domains ():
		print ('Domain:', domain)


# Add a new Resource Collection.
# This adds a collection RDN to LDAP.
# It sets up a default ACL granting no (or "Visitor") rights to anyone.
#
def cmd_collection_add (domain, collname):
	index = reservoir.get_domain (domain)
	reservoir.add_collection (index, collname)
	colluuid = index.get_colluuid ()
	print ('Collection:', colluuid)
	# return colluuid

# Delete a Resource Collection.
# This removes a recourceInstanceKey RDN from LDAP and the bucket in Riak KV
#
def cmd_collection_del (domain, *colluuids):
	idx = reservoir.get_domain (domain)
	for colluuid in colluuids:
		idx.set_colluuid (colluuid)
		reservoir.del_collection (idx)

def cmd_collection_list (domain=None):
	if domain is None:
		domains = reservoir.list_domains ()
	else:
		domains = [domain]
	for domain in domains:
		print ('Domain:', domain)
		index = reservoir.get_domain (domain)
		for colluuid in index.list_index ():
			print ('Collection:', colluuid)

rex_dn_index  = '(?:^|,)resins=([^,]+),associatedDomain=([^,]+),' + reservoir.reservoir_base.replace ('.', '[.]') + '$'
rex_dn_domain =                '(?:^|,)associatedDomain=([^,]+),' + reservoir.reservoir_base.replace ('.', '[.]') + '$'
rex_dn_domusr =    '(?:^|,)uid=([^,]+),associatedDomain=([^,]+),' + reservoir.reservoir_base.replace ('.', '[.]') + '$'

re_dn_index  = re.compile (rex_dn_index )
re_dn_domain = re.compile (rex_dn_domain)
re_dn_domusr = re.compile (rex_dn_domusr)



def cmd_index_add_by_dn (dn1, refname, refcolluuid):
	if dn1 [:7] == 'resins=':
		(colluuid,domain) = re_dn_index.match (dn1).groups ()
		index = reservoir.get_domain (domain)
		index.set_colluuid (colluuid)
	elif dn1 [:4] == 'uid=':
		(userid,domain) = re_dn_domusr.match (dn1).groups ()
		index = reservoir.get_domain_user (domain,userid)
	elif dn1 [:17] == 'associatedDomain=':
		# Fallback to just matching a domain
		(domain,) = re_dn_domain.match (dn1).groups ()
		index = reservoir.get_domain (domain)
		colluuid = index.get_colluuid ()
	else:
		raise Exception ('No index for DN %s' % dn1)
	index [refname] = refcolluuid

def cmd_index_del_by_dn (dn1, refname, refcolluuid):
	(colluuid,domain) = re_dn_index.match (dn1).groups ()
	cmd_index_del (domain, colluuid, refname, refcolluuid)

def cmd_index_del (domain, colluuid, refname, refcolluuid):
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	del index [refname]

def fetch_index_list (dn1):
	if dn1 [:7] == 'resins=':
		(colluuid,domain) = re_dn_index.match (dn1).groups ()
		index = reservoir.get_domain (domain)
		index.set_colluuid (colluuid)
	elif dn1 [:4] == 'uid=':
		(userid,domain) = re_dn_domusr.match (dn1).groups ()
		index = reservoir.get_domain_user (domain,userid)
	elif dn1 [:17] == 'associatedDomain=':
		# Fallback to just matching a domain
		(domain,) = re_dn_domain.match (dn1).groups ()
		index = reservoir.get_domain (domain)
	else:
		raise Exception ('No index for DN %s' % dn1)
	# An index functions like a dict
	return index

def fetch_index_colluid_by_dn_name (dn1, name):
	idx = fetch_index_list (dn1)
	return idx [name]

def cmd_index_list (domain, colluuid):
	print ('Domain:', domain)
	print ('Collection:', colluuid)
	dn1 = 'resins=' + colluuid + ',associatedDomain=' + domain + ',' + base
	for (refname,uuid) in fetch_index_list (dn1).items ():
		print ('Reference:', uuid, refname or '')

# Add an object to the given Resource Collection.
# This sets up a descriptive object in LDAP and loads the object into Riak KV.
#
def cmd_resource_add (domain, colluuid, mediatype, resname, blob):
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	reshdl = reservoir.add_resource (index,
		objectClass=['reservoirResource'],
		uniqueIdentifier=resname,
		mediaType=mediatype,
		cn=resname)
	reskey = reshdl.get_resuuid ()
	with reshdl.open (writing=True, binary=True) as fh:
		fh.write (blob)
	# The hashes are dirty, so commit() the metadata
	reshdl.commit ()
	# print ('Domain:', domain)
	# print ('Collection:', colluuid)
	print ('Resource:', reskey)
	return reskey

# Remove an object.
# This removes the data from Riak KV and the metadata from LDAP.
#
def cmd_resource_del (domain, colluuid, *objkeys):
	idx = reservoir.get_domain (domain)
	idx.set_colluuid (colluuid)
	for objkey in objkeys:
		res = idx.load_resource (res)
		reservoir.del_resource (res)

# List the objects in a Resource Collection.
#
def fetch_resource_list_by_dn (dn1):
	(colluuid,domain) = re_dn_index.match (dn1).groups ()
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	return index.list_resources ()

def fetch_resource_list (domain, colluuid):
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	return index.list_resources ()

def cmd_resource_list (domain=None, colluuid=None):
	if domain is None:
		domains = reservoir.list_domains ()
	else:
		domains = [domain]
	for domain in domains:
		print ('Domain:', domain)
		if colluuid is None:
			colluuids = fetch_collection_list (domain)
		else:
			colluuids = [colluuid]
		for colluuid in colluuids:
			print ('Collection:', colluuid)
			for objkey in fetch_resource_list (domain, colluuid):
				print ('Resource:', objkey)

# Get an object.
# This finds the object in LDAP and subsequently downloads in from Riak KV.
# Or... does it not even need to look into LDAP?
#
resource_attribute_map = {
	'resource': 'Resource',
	'mediaType': 'Content-Type',
	'cn': 'Name',
}
def cmd_resource_get (domain, colluuid, objkey):
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	resobj = index.load_resource (objkey)
	#DEBUG# print ('Query response:', qr1)
	with resobj.open (binary=True) as fh:
		blob = fh.read ()
	al1 = resource_attribute_map.keys ()
	for atnm in al1:
		for atval in resobj.get (atnm, []):
			print ('%s: %s' % (resource_attribute_map [atnm], atval))
	print ('Content-Length', len (blob))
	#TEST# print ('Data:', blob [:100])
	print ('Data:', blob)


# Send an identified object to the listed recipients.
# This finds the object in LDAP and sents it up for sending, without
# overriding sender, timeout or topic, to at least one recipient.
#
def cmd_resource_send (domain, colluuid, objkey, recpts):
	index = reservoir.get_domain (domain)
	index.set_colluuid (colluuid)
	resobj = index.load_resource (objkey)
	if len (recpts) == 0:
		print ('Please specify at least one recipient')
		return False
	resobj.sendto (recpts)


class ListOfDomains (cmdparser.Token):
	def get_values (self, *context):
		#DEBUG# print ('Fetching domain list under context', context, '::', type (context))
		retval = reservoir.list_domains ()
		#DEBUG# print ('Returning domain list', retval)
		return retval
	def __str__ (self):
		return 'domain'


#
# Produce lists of new values (or indicate anything is accepted)
#
def token_factory (token):
	#DEBUG# print ('token_factory ("' + token + '")')
	if token [:3] == 'new':
		# Accept any token (as a newxxx value)
		return cmdparser.AnyToken (token)
	elif token == 'domain':
		# Find a list of domains
		#TODO# return ListOfDomains ()
		retval = ListOfDomains ('domain')
		#DEBUG# print (retval, '::', type (retval))
		return retval
	elif token == 'colluuid':
		# Find a list of collection UUIDs, possibly crossing over to another domain
		"TODO"
		return None
	elif token == 'name':
		# Find a name in the current index (the current one -- confusing "index path")
		"TODO"
		return None
	elif token == 'var=val':
		"TODO"
		return None
	elif token == 'key':
		# Find a name in the current collection
		"TODO"
		return None
	elif token == 'address':
		# Perpahs check an address to have an "@" and maybe lookup contacts -- nah, bad idea
		return None
	elif token == 'descr':
		return cmdparser.AnyTokenString ('descr')
	else:
		# Cry out loud -- this isn't supposed to happen
		raise NotImplementedError ('Unknown kind of token in syntax: ' + token)


#
# The shell for arpa2reservoir (based on cmdparser)
#
#TODO# @cmdparser.CmdClassDecorator()
class Cmd (cmdshell.Cmd):

	version = (0,0)
	prompt = "arpa2reservoir> "
	intro = "Edit Reservoir: Resource Collections and Resources.\nAnd Resource Indexes per Domain and per Resource Collection."

	def __init__ (self):
		cmdshell.Cmd.__init__ (self)
		self.cur_domain = None
		self.cur_dn = None
		#UNUSED# self.cur_colluuid = None

	def reset (self):
		self.cur_domain = None
		self.cur_dn = None

	@cmdparser.CmdMethodDecorator(token_factory=token_factory)
	def do_domain (self, args, fields):
		"""
		domain ( list | add <newdomain> | [tree] del <domain> [...] )

		domain list -- Lists domains managed under the Reservoir.
		domain add -- Creates a new domain under the Reservoir.
		domain del -- Removes an empty domain from the Reservoir
		"""

		if args [1] == 'list':
			for domain in reservoir.list_domains ():
				print (domain)
		elif args [1] == 'add':
			cmd_domain_add (args [2])
			self.cur_domain = args [2]
		elif args [1] == 'del':
			cmd_domain_del (False, *args [2:])
			if self.cur_domain in args [2:]:
				self.cur_domain = None
		elif args [1] == 'tree' and args [2] == 'del':
			cmd_domain_del (True, *args [3:])
			if self.cur_domain in args [3:]:
				self.cur_domain = None
		else:
			raise NotImplementedError ('Unexpected command ' + ' '.join (args))
		return False

	@cmdparser.CmdMethodDecorator(token_factory=token_factory)
	def do_index (self, args, fields):
		"""
		index ( dn | list | add <colluuid> <newname> | del <name> |
		        domain <domain> | domainuser <domain> <newname> |
		        collection <colluuid> | path <name> [...] )

		index dn -- Shows the LDAP node with the current index.
		index list -- Lists the (<colluid> and) <name> for index entries.
		index add -- Adds a <newcolluid> to the index with a new <newname>.
		index del -- Removes a <name> from the index.
		index domain -- Changes the current index to that of the <domain>.
		index domainuser -- Changes the index to the <domain> and its user <name>.
		index collection -- Changes the current index to a given <colluuid>.
		index path -- Steps from index to index following the <name> in each.
		"""

		if args [1] == 'dn':
			if self.cur_dn is not None:
				print (self.cur_dn)
			else:
				print ('(undefined)')
		elif args [1] == 'list':
			if self.cur_dn is None:
				print ('First use: index domain <domain> | collection <colluuid>')
				return False
			idx = fetch_index_list (self.cur_dn)
			for (k,v) in idx.items ():
				if k is None:
					k = '(default)'
				print (k + '\t-> ' + v)
		elif args [1] == 'add':
			if self.cur_dn is None:
				print ('First use: index domain <domain> | collection <colluuid>')
				return False
			cmd_index_add_by_dn (self.cur_dn, args [3], args [2])
		elif args [1] == 'del':
			if self.cur_dn is None:
				print ('First use: index domain <domain> | collection <colluuid>')
				return False
			colluuid = fetch_index_colluid_by_dn_name (self.cur_dn, args [2])
			cmd_index_del_by_dn (self.cur_dn, args [2], colluuid)
			#UNUSED# if colluuid == self.cur_colluuid:
			#UNUSED# 	self.cur_colluuid = None
		elif args [1] == 'domain':
			self.cur_dn = 'associatedDomain=' + args [2] + ',' + reservoir.reservoir_base
			self.cur_domain = args [2]
			#UNUSED# self.cur_colluuid = None
		elif args [1] == 'domainuser':
			self.cur_dn = 'uid=' + args [3] + ',' + 'associatedDomain=' + args [2] + ',' + reservoir.reservoir_base
			self.cur_domain = args [2]
			self.cur_user = args [3]
		elif args [1] == 'collection':
			if self.cur_domain is None:
				print ('First use: index domain <domain>')
				return False
			self.cur_dn = 'resins=' + args [2] + ',' + 'associatedDomain=' + self.cur_domain + ',' + reservoir.reservoir_base
		elif args [1] == 'path':
			if self.cur_dn is None:
				print ('First use: index domain <domain> | collection <colluuid>')
				return False
			domain_and_up = ',associatedDomain=' + self.cur_domain + ',' + reservoir.reservoir_base
			for leg in args [2:]:
				idx = fetch_index_list (self.cur_dn)
				print ('Looking up', leg, '::', type (leg), 'in', idx)
				if leg not in idx:
					print ('Did not find', leg)
					break
				print (leg + '\t-> ' + idx [leg])
				self.cur_dn = 'resins=' + idx [leg] + domain_and_up
			print ('Current DN is now', self.cur_dn)
		else:
			raise NotImplementedError ('Unexpected command ' + ' '.join (args))
		return False

	@cmdparser.CmdMethodDecorator(token_factory=token_factory)
	def do_collection (self, args, fields):
		"""
		collection ( list [<domain>] | add <domain> <descr> | del <domain> <colluuid> [...] )

		collection list -- Shows the indexed collection, possibly for a <domain>.
		collection add -- Adds a collection described as <descr> to domain <domain>.
		collection del -- The indicated <colluid>s under <domain> are removed.
		"""

		if args [1] == 'list':
			#TODO# Currently lists everything, that could get a bit much!
			#TODO# What does this add to "index list" and "resource list"?
			cmd_collection_list (args [2] if len (args)>=3 else None)
		elif args [1] == 'add':
			cmd_collection_add (args [2], args [3])
		elif args [1] == 'del':
			cmd_collection_del (args [3], *args [4:])
			#UNUSED# if self.cur_colluuid in args [4:]:
			#USUSED# 	TODO
		else:
			raise NotImplementedError ('Unexpected command ' + ' '.join (args))
		return False

	@cmdparser.CmdMethodDecorator(token_factory=token_factory)
	def do_resource (self, args, fields):
		"""
		resource ( list
		         | add <var=val> [...]
		         | del <key> [...]
		         | get <key>
		         | send <key> <address> [...] )

		To be added:
		         | set <key> <var=val> [...]

		resource list -- Shows the resources in the current collection.
		resource add  -- Adds a resource to the current collection.
		resource del  -- Removes a resource from the current collection.
		resource get  -- Retrieves the resource and its metadata.
		resource send -- Sends the resources to one or more address.

		Resource adding requires metadata in var=val parameters,
		at least type=... name=... file=... are required.  (TODO)
		"""

		if args [1] == 'list':
			if self.cur_dn is None:
				print ('First use: index collection <colluuid>')
				return False
			for objkey in fetch_resource_list_by_dn (self.cur_dn):
				print ('Resource:', objkey)
		elif args [1] == 'add':
			m = re_dn_index.match (self.cur_dn or '')
			if m is None:
				print ('First use: index collection <colluuid>')
				return False
			(colluuid,domain) = m.groups ()
			vars = { }
			for var_val in args [2:]:
				(var,val) = var_val.split ('=', 1)
				vars [var] = val
			print ('VARS =', vars)
			mediatype = vars.get ('type')
			objname = vars.get ('name')
			filename = vars.get ('file')
			print ('TYPE/NAME/FILE =', (mediatype,objname,filename))
			if not (mediatype and objname and filename):
				print ('Please specify at least type= name= file=')
				return False
			blob = open (filename, 'rb').read ()
			#TODO# Future extension possible with more attributes
			cmd_resource_add (domain, colluuid, mediatype, objname, blob)
		elif args [1] == 'del':
			m = re_dn_index.match (self.cur_dn or '')
			if m is None:
				print ('First use: index collection <colluuid>')
				return False
			(colluuid,domain) = m.groups ()
			cmd_resource_del (domain, colluuid, args [2:])
		elif args [1] == 'get':
			m = re_dn_index.match (self.cur_dn or '')
			if m is None:
				print ('First use: index collection <colluuid>')
				return False
			(colluuid,domain) = m.groups ()
			cmd_resource_get (domain, colluuid, args [2])
		elif args [1] == 'send':
			m = re_dn_index.match (self.cur_dn or '')
			if m is None:
				print ('First use: index collection <colluuid>')
				return False
			(colluuid,domain) = m.groups ()
			cmd_resource_send (domain, colluuid, args [2], args [3:])
		else:
			raise NotImplementedError ('Unexpected command ' + ' '.join (args))
		return False

def main ():
	shell = Cmd ()
	shell.cmdloop ()

if __name__ == '__main__':
	main ()

