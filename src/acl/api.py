# API module for arpa2.acl including arpa2.identity
#
# Note: There will be two approaches; one directly on LDAP
#       and another working more remotely on a local DB.
#       These will not be completely the same, and the code
#       may have to be split over them.  Currently unclear.


import re
import copy
import struct

import hashlib

import urllib.parse



# The complete NAI syntax is defined in Section 2.1 of RFC 7542.
#
# nai ::= utf8-username | "@" utf8-realm | utf8-username "@" utf8-realm
#
# We slightly modify (and actually simplify) it to
#
# arpa2-nai ::= [ utf8-username "@" ] utf8-realm
#
# This form is more in line with user-facing protocols than the
# official NAI that is geared towards RADIUS; they do translate
# easily, however, as all internal patterns are the same.
#
# Use re_nai      for the compiled regex form of plain NAI, or
# use re_arpa2nai for the compiled regex form of our  NAI.
#
def _chrs (fst,lst):
	return b'[\\x%02x-\\x%02x]' % (fst,lst)
_rex_tail = _chrs(0x80,0xbf)
_rex_utf8_2 = _chrs(0xc2,0xdf) + _rex_tail
_rex_utf8_3 = b'(?:%s|%s|%s|%s)' % (
		_chrs(0xe0,0xe0) + _chrs(0xa0,0xbf) + _rex_tail,
		_chrs(0xe1,0xec) + _rex_tail        + _rex_tail,
		_chrs(0xed,0xed) + _chrs(0x80,0x9f) + _rex_tail,
		_chrs(0xee,0xef) + _rex_tail        + _rex_tail )
_rex_utf8_4 = b'(?:%s|%s|%s)' % (
		_chrs(0xf0,0xf0) + _chrs(0x90,0xbf) + _rex_tail + _rex_tail,
		_chrs(0xf1,0xf3) + _rex_tail        + _rex_tail + _rex_tail,
		_chrs(0xf4,0xf4) + _chrs(0x80,0x8f) + _rex_tail + _rex_tail )
_rex_utf8_xtra_char = b'(?:%s|%s|%s)' % (_rex_utf8_2, _rex_utf8_3, _rex_utf8_4)
_rex_string = b'(?:(?:[a-zA-Z0-9\!\#\$%%\&\'\*\+\-/=\?\^_\`\{\|\}\~]|%s)+)' % _rex_utf8_xtra_char
_rex_username = b'(?:%s(?:[.]%s)*)' % (_rex_string, _rex_string)
_rex_utf8_rtext = b'(?:[a-zA-Z0-9]|%s)' % _rex_utf8_xtra_char
_rex_ldh_str = b'(?:(?:%s|[-])*%s)' % (_rex_utf8_rtext, _rex_utf8_rtext)
_rex_label = b'(?:%s(?:%s)*)' % (_rex_utf8_rtext, _rex_ldh_str)
_rex_realm = b'(?:%s(?:[.]%s)+)' % (_rex_label, _rex_label)
_rex_nai = b'(?:%s(?:[@]%s)?|[@]%s)' % (_rex_username, _rex_realm, _rex_realm)
#DEBUG# print ('_rex_nai = %r' % _rex_nai)

_rex_arpa2nai = b'(?:(%s)@)?(%s)' % (_rex_username, _rex_realm)
#DEBUG# print ('_rex_arpa2nai = %r' % _rex_arpa2nai)


"""The re_arpa2nai is produced by re.compile() to split a correct
   ARPA2-modified NAI into a local-part and a domain/realm part,
   where the local-part is optional.  Its syntax is defined by
   
   arpa2-nai = [ utf8-username "@" ] utf8-realm
   
   Where utf8-username and utf8-realm are terms from RFC 7542.
   The definitions are complex, but reject anything with
   illegal symbols such as 0x00 and 0xff, illegal sequences
   such as 0xe0 0x4a, and sequences that are not as short
   as they could be.
"""
re_nai      = re.compile (b'^%s$' % _rex_nai     )
re_arpa2nai = re.compile (b'^%s$' % _rex_arpa2nai)



# Now an identity needs to be a proper NAI _and_ follow our restrictions.
# This means that we do not accept all NAI forms.  Though we swallow most.
#
# The regex below is designed to capture meaningful portions of addresses
# and return groups with their values.  This ought to be the most efficient
# parsing Python can do; it may even share memory as a slice.
#
# The signature format looks funny; [q-z2-7]* all have the high bit set
# to flag continuation; the last flag byte is [a-p] to discontinue the
# flags.  This saves the tight space from an extra + separator.

_rex_id = b'(?:[^@+]+)'
_rex_sig = b'[+](?P<sigflags>[q-z2-7]*[a-p])(?P<signature>(?P<exptime>[q-z2-7]*[a-p])?(?P<sigreal>[a-z2-7]*))?[+]'
_rex_human = b'(?:(?P<basename>' + _rex_id + b')(?:[+](?P<aliases>(?:' + _rex_id + b')(?:[+]' + _rex_id + b')*))?)'
_rex_bot = b'(?:[+](?P<botname>' + _rex_id + b')(?:[+](?P<botargs>(?:' + _rex_id + b')(?:[+]' + _rex_id + b')*))?)'
_rex_arpa2components = b'(?:' + _rex_human + b'|' + _rex_bot + b')(?:' + _rex_sig + b')?(?:@[^@]*)?'


"""The re_arpa2components is produced by re.compile() to further
   split the local-part of an approved arpa2-nai into components
   for which ARPA2 projects define special semantics; the output
   is either None for failure, or a dictionary.  The pattern may
   operate on just a local-part or on an entire arpa2-nai.
   
   The informal syntax below shows all "+" delimiters explicitly,
   and ignores any part after "@".
   
   arpa2-nai ::=    <basename> 0*( "+" <alias>  ) [ "+" <sigflags> "+" [ <signature> ] "+" ]
               / "+" <botname> 0*( "+" <botarg> ) [ "+" <sigflags> "+" [ <signature> ] "+" ]
   
   The <aliases> and <botargs> are not provided separately, but
   as a concatenation of "+" prefixed words.
"""
re_arpa2components = re.compile (b'^%s$' % _rex_arpa2components)


def b2s (b):
	if b is None:
		return b
	else:
		return str   (b, 'utf-8')

def s2b (s):
	if s is None:
		return s
	else:
		return bytes (s, 'utf-8')



# Support for base-32 encoding/decoding
# Following RFC 4648

_base32tab = 'abcdefghijklmnopqrstuvwxyz234567'

def _int_to_base32 (intval, digits):
	retval = ''
	for d in range (digits):
		c = (intval >> (5*d)) & 0x1f
		retval = _base32tab [c] + retval
	return retval

def _base32_to_int (strval):
	retval = 0
	for c in strval:
		retval <<= 5
		retval += _base32tab.index (c)
	return retval

def _reverse_key (pad64):
	for i in range (len (pad64 or [])):
		pad64 [i] = -pad64 [i]
	return pad64


# The lowercase characters that we want to encrypt
# Note: For i18n, we simply cycle the UTF-8 bits

_cryptchars = b'abcdefghijklmnopqrstuvwxyz0123456789'



class Identity:
	"""An ARPA2 Identity is a string, with internationalisation
	   support.  It is a complete specification of users, groups,
	   services and so on, always under a domain.  Just a domain
	   is also usable as an identity.
	   
	   The syntax of the local-part an ARPA2 identity:
	   
	    * Can start with `+` to identify a service/bot
	    * Can end with `+SIG+` to include a signature `SIG`
	    * Can use intermediate `+` to append aliases
	   
	   Signatures are newly introduced with ARPA2 Identities.
	   They are based on scraps of information from the context,
	   such as message components or the time, so these elements
	   can be constrained in their use.  Your email address may
	   be made available only to a sender address, or it may
	   expire after 3 weeks, for example.
	   
	   When an arpa2.Identity is printed, it is completely in
	   lowercase, with the possible exception of a signature,
	   which we habitually write in uppercase (as a warning)
	   though there is no formal reason for it -- the code is
	   base32, and case-insensitive.
	   
	   The object contains either a basename or a botname;
	   a basename may have a list aliases or, if none were
	   present, None.  The same for botargs after botname.
	   All forms may have a signature.
	   
	   The basic elements of localpart and domain are also
	   available as attributes, as well as the entire identity.
	   When you modify this objects, these three should be
	   regenerated with sync() before you can use them, as
	   they will be outdated.  This will not be the case
	   with the str() output -- that is in fact how the
	   combined values are reformulated.
	"""

	def __init__ (self, identity=None, domain=None, escaped=False):
		"""Parse the identity input.  If it is a local-part only,
		   then domain can be appended.
		   
		   If the escaped flag is set, any percent encoding is
		   removed from the identity.  This is never done for
		   the domain.
		   
		   When the identity is bytes, either due to percent
		   escape decoding or supplied as such, then it is
		   interpreted as an UTF-8 notation and converted to
		   str.  This involves a syntax check of the bytes
		   that may raise an exception.
		   
		   When the domain is provided, it is appended to the
		   identity after a "@" separator.  The identity must
		   not have no more than one "@", no matter where from.
		   
		   The identity is syntax-checked to almost conform to
		   the NAI form in RFC 7542.  The changes are that a
		   lone utf8-username is not permitted, and that the
		   form of an utf8-realm without utf8-username does
		   not start with "@":
		   
		   arpa2-nai = [ utf8-username "@" ] utf8-realm
		   
		   The various interpretations of the use of "+" in
		   the local-part are specific to ARPA2 projects, and
		   this class supports questions about them:
		   
		   localpart, domain --> before and after "@"
		   service, args --> localpart starts "+" split on "+"
		   basename, alias0, aliases --> localpart split on "+"
		   signature --> localpart trailing signature
		"""
		assert identity is not None or domain is not None, 'You cannot be nobody'
		assert domain is not None or identity is None or '@' in identity, 'Need a domain'
		if escaped:
			assert identity is not None, 'Only user names can be escaped'
			assert type (identity) == str, 'Percent escapes are only removed from strings'
			identity = urllib.parse.unquote (identity)
		if identity is None or identity == '':
			identity = domain
		elif domain is not None:
			identity = '%s@%s' % (identity,domain)
		identity = identity.lower ()
		print ('DEBUG: identity is %r' % identity)
		m = re_arpa2nai.match (bytes (identity, 'utf-8'))
		if m is None:
			raise Exception ('Syntax error in ARPA2 Identity')
		print ('DEBUG: (local,domain) is %r' % (map (b2s, m.groups ()),))
		self.identity = identity
		(self.localpart,self.domain) = map (b2s, m.groups ())
		n = re_arpa2components.match (s2b (self.localpart))
		if n is None:
			raise Exception ('Syntax error in ARPA2 Identity/Signature')
		print ('DEBUG: groups is %r' % n.groupdict ())
		(self.basename, self.aliases, self.botname, self.botargs, self.sigflags, self.signature, self.exptime, self.sigreal) = map (b2s, n.groups ())
		if self.aliases is not None:
			self.aliases = self.aliases.split ('+')
		if self.botargs is not None:
			self.botargs = self.botargs.split ('+')
		print ('DEBUG: object is %r' % ((self.basename, self.aliases, self.botname, self.botargs, self.sigflags, self.signature),))
		print ('DEBUG.')

	def __str__ (self):
		"""Compose the components into a string.  Components are
		   basename, aliases, botname, botargs, signature, domain.
		   The form localpart is composed on sync().
		"""
		if self.basename is not None:
			retval = self.basename
			if self.aliases is not None:
				retval = retval + '+' + '+'.join (self.aliases)
		else:
			retval = '+' + self.botname
			if self.botargs is not None:
				retval = retval + '+' + '+'.join (self.botargs)
		if self.sigflags is not None:
			retval = retval + '+' + self.sigflags.upper () + self.signature.upper () + '+'
		retval = retval + '@' + self.domain
		return retval

	def sync (self):
		"""Bring the self.identity and self.localpart back in line with
		   the other parameters which may be changed.  No syntax checks
		   are performed, your code is assumed to behave correctly.
		"""
		self.identity = str (self)
		(self.localpart,self.domain) = self.identity.split ('@')

	def selectors (self):
		"""Generate the possible Selector values for this Identity,
		   in the proper order for matching, and for finding the
		   closest possible match first.  Note that we do not treat
		   the signature special in any way; Selectors are used on
		   the identities of _peers_, and signatures are only of
		   interest to verify that _our_ identity is correclty
		   addressed.  So, other than reproducing it, we do not do
		   much here.  An ACL will not be able to require signed
		   peers; it would not be able to check the signatures.
		   The interpretation of "+" in remote peers as an alias
		   is less far-fatched, and directly useful because some
		   peers may use this same pattern, from ARPA2 or other.
		"""
		localpart = str (self)
		localpart = localpart [:localpart.find ('@')]
		firstplus = localpart.find ('+',1)
		if firstplus == -1:
			# Plain users can step up to their alias
			localpart_addalias = localpart + '+'
			locs = [ localpart, localpart_addalias, '' ]
		else:
			# The ACL can step down to accept any alias
			localpart_delalias = localpart [:firstplus+1]
			locs = [ localpart, localpart_delalias, '' ]
		domp = self.domain
		while True:
			for locs0 in locs:
				yield locs0 + '@' + domp
			if domp == '.':
				break
			didx = domp.find ('.',1)
			if didx == -1:
				domp = '.'
			else:
				domp = domp [didx:]


	def _compute_signature (self, sigchars, exptime, context):
		"""Compute a signature, possibly with expiration time.
		   The return value is a (signature,encryption_key)
		   where the signature has length sigchars and the
		   encryption_key provides 64 offsets to cycle the
		   respective local-part characters in _cryptchars,
		   forward to encrypt and backward to decrypt.
		   
		   TODO: This code ought to be written in C, not Python.
		         At best, this code is a reference implementation
		         and fallback option.
		"""
		def flag32 (i):
			code = self.sigflags [i].lower ()
			return _base32_to_int (code)
		def dohash (h,s):
			if type (s) == str:
				s = bytes (s, 'utf-8')
			if len (s) > 65535:
				raise ValueError ('Signature parameter exceeds 65535 bytes')
			h.update (struct.pack ('!H', len (s)))
			h.update (s)
		# Initialise variables, start hashing (local choice SHA-256)
		sig = ''
		flags = flag32 (0)
		h0 = hashlib.sha256 ()
		# Insert key material
		h0.update (b'TODO:KEY MATERIAL')
		# Target signature length in bits (TODO: DOCUMENTATION)
		# Note: Includes sigtime but not sigflags; consistency matters
		h0.update (struct.pack ('!H', sigchars * 5))
		# Expiration time
		if flags & 0x01 != 0x00:
			assert len (exptime) == 5
			dohash (h0, exptime)
			sig += exptime
		# Domain of remote Identity
		if flags & 0x02 != 0x00:
			dohash (h0, context ['remoteIdentity'].domain)
		# LocalPart of remote Identity
		if flags & 0x04 != 0x00:
			dohash (h0, context ['remoteIdentity'].localpart)
		# Aliases of remote Identity (TODO: SKIP IF NONE)
		if flags & 0x08 != 0x00:
			if context ['remoteIdentity'].aliases is not None:
				dohash (h0, '+'.join (context ['remoteIdentity'].aliases))
		# Following flag byte
		if flags & 0x10 != 0x00:
			flags = flag32 (1)
		else:
			flags = 0
		# Machine-generated Session Identifier
		if flags & 0x01 != 0x00:
			dohash (h0, context ['sessionId'])
		# Human-entered Subject
		if flags & 0x02 != 0x00:
			dohash (h0, context ['humanSubject'])
		# Automation-friendly Topic/Tag in Subject
		if flags & 0x04 != 0x00:
			dohash (h0, context ['automationTopic'])
		# Encrypted Localpart
		do_encrypt = flags & 0x08 !=0x00
		# Following flag byte
		if flags & 0x10 != 0x00:
			raise ValueError ('Signature includes unknown flags')
		# Clone and derive multiple outputs:
		#  - signature, extended long enough for local-part
		#  - 64 pseudo-random bytes for localpart cycling
		#  - The maxlen of the local-part is 64, that's why
		#  - To ensure bitsize, we need 2 rounds of SHA-256
		#  - Signatures calculation is simpler with 2 rounds
		#TODO# DOCUPDATE, TAKE BITS FROM OUTPUT BYTE NO SHIFT
		def finish (h, act, blknum):
			h.update (b'-----ARPA2 IDENTITY %s BLOCK %d-----' % (act,blknum))
			return h.digest ()
		h1 = h0.copy ()
		h2 = h0.copy ()
		h3 = h0.copy ()
		pre_sig = finish (h0, b'SIGNATURE' , 0) + finish (h1, b'SIGNATURE' , 1)
		pre_enc = finish (h2, b'ENCRYPTION', 0) + finish (h3, b'ENCRYPTION', 1)
		# Now fill up to "sigchars" with base32 digits from pre_sig
		sigchars -= len (sig)
		for i in range (sigchars):
			sig += _int_to_base32 (pre_sig [i], 1)
		# Form 64 padding codes from pre_enc, or None for no encryption
		if do_encrypt:
			pad64 = [ c % len (_cryptchars) for c in pre_enc ]
		else:
			pad64 = None
		#TODO# Signature calculation, padding calculation
		return (sig,pad64)

	def _crypt_localpart (self, pad64):
		"""Perform encryption (or after _reverse_key() decryption)
		   on the various strings in the Identity local-part.  Then
		   run sync() to gather the larger pieces together.  The
		   parts that are encrypted are basename, aliases, botname
		   and botargs.  The characters are affected are the ones
		   in _cryptchars, so the "+" character and other symbolic
		   characters are not affected; a valid NAI translates to
		   a valid NAI and the arpa2.Identity parser cuts the same
		   portions from the address.
		   
		   The assumptions behind this encryption are weak, with
		   key material derived from the protocol context; this
		   usually means that the material is publicly available.
		   On the other hand, when a signature fails, so when the
		   context has been changed in an unacceptable manner, it
		   is usually radically different and the party trying to
		   abuse the identity is unaware of the context.  In that
		   situation, the privacy of the localpart is protected,
		   albeit with simple crypto based on entropy that might
		   be recovered with some perseverance.
		   
		   TODO: This code ought to be written in C, not Python.
		         At best, this code is a reference implementation
		         and fallback option.
		"""
		if pad64 is None:
			return
		def labelmap (l,i):
			retval = b''
			if type (l) == str:
				l = bytes (l, 'utf-8')
			for (i0,l0) in enumerate (l, i):
				if l0 & 0x80 == 0x00:
					# ASCII table
					#TODO# Use a dict, not _cryptchars.find() and lower()
					if 65 <= l0 <= 90:
						l0 -= 32
					p0 = _cryptchars.find (l0)
					if p0 == -1:
						retval += bytes ([l0])
						continue
					p0 += pad64 [i0]
					p0 %= len (_cryptchars)
					retval += bytes ([_cryptchars [p0]])
				elif l0 & 0xc0 == 0x80:
					# UTF-8 trailer, cycle 6 bits
					retval += bytes ([((l0 + pad64 [i0]) & 0x3f) | 0x80])
				elif l0 & 0xe0 == 0xc0:
					# UTF-8 intro for 2-byte form, cycle 5 bits
					retval += bytes ([((l0 + pad64 [i0]) & 0x1f) | 0xc0])
				elif l0 & 0xf0 == 0xe0:
					# UTF-8 intro for 3-byte form, cycle 4 bits
					retval += bytes ([((l0 + pad64 [i0]) & 0x0f) | 0xe0])
				elif l0 & 0xf8 == 0xf0:
					# UTF-8 intro for 4-byte form, cycle 3 bits
					retval += bytes ([((l0 + pad64 [i0]) & 0x07) | 0xf0])
				else:
					# Funny case, should not happen, skip crypto
					retval += bytes ([l0])
			return str (retval, 'utf-8')
		if self.basename is not None:
			self.basename = labelmap (self.basename, 0)
			pos = 0 + len (self.basename)
		if self.botname is not None:
			self.botname  = labelmap (self.botname,  1)
			pos = 1 + len (self.botname)
		if self.aliases is not None:
			args = self.aliases
		elif self.botargs is not None:
			args = self.botargs
		else:
			args = []
		for i in range (len (args)):
			args [i] = labelmap (args [i], 1 + pos)
			pos += 1 + len (args [i])


	def update_signature (self, **context):
		"""Create or update the (size-limited) signature at the
		   end of an email addres.  Based on the included context,
		   the computation can be more or less dependent on the
		   environment.  You will have to provide most of these
		   context again when you check the signature; the values
		   are chosen to allow you to do so simply, based on the
		   application context that you intend to constrain, but
		   that is not directly accessible to this generic code.
		   Your text may be either bytes or Python3 str; in the
		   latter case, it will be encoded to UTF-8 before it is
		   hashed.  Avoid texts over 65535 bytes, as these will
		   be rejected in any case.  Flags are simply present
		   to indicate True.
		   
		   The context to provide includes:
		   
		   remoteIdentity: Identity object for the remote peer;
		   
		   expirationDays: Days good until signature expiration;
		   
		   remoteUseDomain: Flag to use the remote peer domain;
		   
		   remoteUseLocalpart: Flag to use remote peer localpart;
		   
		   remoteUseAliases: Flag to use remote peer aliases;
		   
		   sessionId: Machine-generated session identifier, is
		              usually dependent on the protocol;
		   
		   humanSubject: Precisely the subject entered by humans
		                 but possibly stripped and simplified to
		                 remove obvious problems and perhaps some
		                 protocol changes like "Re:" and "(fwd)";
		   
		   automationTopic: Automation-friendly subject [tag];
		   
		   encryptedLocalPart: Flag to encrypt the local peer
		                       localpart (before signature).
		"""
		flagsmap = {
			'sessionId':          0x0001,
			'humanSubject':       0x0002,
			'automationTopic':    0x0004,
			'encryptedLocalPart': 0x0008,
			'expirationDays':     0x0020,
			'remoteUseDomain':    0x0040,
			'remoteUseLocalPart': 0x0080,
			'remoteUseAliases':   0x0100,
		}
		sigflags = 0x0000
		for (f,v) in flagsmap.items ():
			if f in context:
				sigflags += v
		# if sigflags & 0x1f != 0x00:
		if True:
			sigflags += 0x0200
		self.sigflags = _int_to_base32 (sigflags, 2)
		exptime = _int_to_base32 (100, 5)
		sigchars = 2 + 5 + 22
		(sig,pad64) = self._compute_signature (sigchars, exptime, context)
		self.signature = sig
		#TODO# any 2nd round should first decrypt (with check_signature)
		self._crypt_localpart (pad64)


	def check_signature (self, **context):
		"""Check the light-weight signature that was added by
		   a prior call to update_signature().  The intention
		   of this check is to ensure that the same protocol
		   context is present, looking at the aspects described
		   for update_signature().  To this end, at least the
		   parameters presentend then must be provided.  The
		   parameters should be obtained from the context, and
		   involve such things as the remote peer Identity and
		   headers or tags.
		   
		   If no signature was added to the local Identity,
		   then the check terminates immediate, so the check
		   is efficiently included in any flow.  It is usually
		   complementary to an ACL check, which involves the
		   remote Identity whereas a signature verifies that
		   a valid local Identity is being used.  Users with
		   no other knowledge of local identities will have
		   to use these identities.  When the check fails, an
		   exception will be raised.  This also happens when
		   the context failed to provide all necessary input.
		"""
		if self.sigflags is None:
			return
		sigchars = len (self.signature)
		exptime = self.signature [:5]
		(soll,pad64) = self._compute_signature (sigchars, exptime, context)
		if self.signature != soll:
			print ('DEBUG: Ist  %s', self.signature)
			print ('DEBUG: Soll %s', soll)
			raise ValueException ('Signature mismatch in %s' % self)
			pass
		_reverse_key (pad64)
		self._crypt_localpart (pad64)


class AccessControlList:
	"""Access Control List (or ACL) objects represent the right to access
	   some resource.  An authenticated identity is compared to the ACL
	   entries to determine their rights.  The closest match wins, so there
	   is a possibility to reject things very broadly and zoom in on a few
	   specific parties to welcome.  There is a most general right, which
	   is usually the most restrictive form of access.
	   
	   Once setup, an ACL can be queried for the rights of any given user
	   identity.  It quickly moves up in a hierarchy to determine the
	   desired rights in just a few steps.
	   
	   For ARPA2, we express access rights as a character, and a set of
	   these is a string.  When asked for the ACL outcome, this string
	   is returned.  To learn if right 'W' is on there, simply test if
	   ('W' in rights) holds.  The mechanism being efficient, you are
	   advised to refresh regularly.
	"""

	def __init__ (self, acl_strings, no_rights=''):
		"""Parse the ACL string(s) and turn them into the fast lookup
		   structures of this ACL.  When not replaced, the no_rights
		   is setup to grant an empty string, so no rights at all, to
		   the most general patterns (that is '@.' for users and '.'
		   for domains).
		   
		   The string format for ACL is defined in the LDAP schema as
		   the acl attribute type.  We repeat most of it here:
		   
		   # accessControlList describes a number of properties in a
		   # space separated string:
		   #       lineno level[ selector...]
		   # where lineno is a non-negative integer in decimal text
		   # representation, indicating the desired order of display,
		   # level is a required word starting with a letter, and a
		   # series of 0 or more space-prefixed DoNAI Selectors
		   # indicates what to match against to reach the intended
		   # level.
		   #
		   # When no DoNAI Selector is used, it really means an empty list,
		   # so no matches at all.  This may be useful for transient uses
		   # and has not been assigned any special meaning.  It would be
		   # really UNIXy, and really confusing, to use such a notation for
		   # a default, for example.  This shall not be done because it is
		   # quite possible to express defaults with catch-all Selectors.
		   
		   The lineno is really just ornamental.  If you specify the
		   same selector in multiple places, you are asking for trouble.
		   Note that the format is implemented below to allow overriding
		   the level within the line, as this is not more complex and
		   in fact a convenience.  We do require continued support for
		   multiple-line specifications, however.
		"""
		self.rights = { }
		self.txnrights = None
		self.txn_begin ()
		self.txnrights = { '.': no_rights, '@.': no_rights }
		if type (acl_strings) in (str,bytes):
			acl_strings = [acl_strings]
		for acls in acl_strings:
			if type (acls) == bytes:
				acls = str (acls, 'utf-8')
			if not ' ' in acls:
				continue
			acls = acls.split (' ') [1:]
			sofar = no_rights
			print ('DEBUG: ACL: Adding', acls)
			for acls0 in acls:
				if acls0 [:1] == '%' and acls0 [:2] != '%%':
					sofar = acls0 [1:]
					continue
				if acls0 [:2] == '%%':
					# Escaped initial percent sign
					acls0 = acls0 [1:]
				self.txnrights [acls0] = sofar
		self.txn_commit ()
		print ('DEBUG: Parsed the ACL into\n%r' % self.rights)

	def check_rights (self, identity):
		"""Given an arpa2.Identity, check its closest match with this
		   ACL to determine its access rights.  The no_rights value
		   used during object initialisation will be returned when
		   no matches were found at all.
		"""
		for id0 in identity.selectors ():
			print ('DEBUG: Testing for', id0)
			if id0 in self.rights:
				return self.rights [id0]
		assert False, 'Fallback selector not present in ACL'

	def notify_changes (self, list_selector_revoked_granted):
		"""This is an abstract method that subclasses may override
		   if they want to be notified on changes to the ACL.  This
		   can be useful to immediately retract rights granted on
		   sessions that are currently running.  Nothing beats the
		   feeling of control but cutting down on a live session ;-)
		   The parameter is a list of tuples with the selector that
		   changed, along with the revoked and granted rights.  This
		   is the outcome of a transaction, so it is all one large
		   atomic change.  The revoked and granted rights are strings
		   that may be empty if nothing changed.
		   
		   The changes made during object initiation will also be
		   reported through notify_change().
		"""
		pass

	def txn_begin (self):
		"""Start a transaction to change rights in one atomic swoop.
		"""
		if self.txnrights is not None:
			raise Exception ('You already started a transaction')
		self.txnrights = copy.deepcopy (self.rights)

	def txn_clear (self):
		"""Wipe the rights database as part of a transaction, so it
		   may be built from basics.
		"""
		if self.txnrights is None:
			raise Exception ('You are not in a transaction')
		self.txnrights = { }

	def txn_grant (self, selector, rights):
		"""Grant the named rights to the named selector.  The
		   rights are a string of characters that each represent
		   a right to exercise.
		"""
		if self.txnrights is None:
			raise Exception ('You are not in a transaction')
		if selector not in self.txnrights:
			self.txnrights [selector] = rights
		else:
			cur = set (self.txnrights [selector])
			new = set (rights)
			self.txnrights [selector] = ''.join (cur.union(new))

	def txn_revoke (self, selector, rights):
		"""Revoke the named rights from the named selector.  The
		   rights are a string of characters that each represent
		   a right to withdraw.
		"""
		if self.txnrights is None:
			raise Exception ('You are not in a transaction')
		if selector in self.txnrights:
			cur = set (self.txnrights [selector])
			new = set (rights)
			if cur == new:
				del self.txnrights [selector]
			else:
				self.txnrights [selector] = ''.join (cur.difference(new))

	def txn_rollback (self):
		"""Do not continue with the transaction.  It is quietly
		   allowed to do this when no transaction is active.
		"""
		self.txnrights = None

	def txn_prepare (self):
		"""Prepare to commit the transaction.  Return True on
		   success or False on failure.  Note that there are no
		   reasons in this version to ever deny this simply
		   object-internal change.  This is the advantage of
		   ruling the World of Access Rights, supposedly.  It
		   is quietly allowed to do this when no transaction
		   is active.
		"""
		return True

	def txn_commit (self):
		"""Commit the transaction.  After this is done, report
		   to lower layers about the change.  Do not return
		   from this call until the lower layer returns from
		   the notification.  It is quietly allowed to do this
		   when no transaction is active.
		"""
		if self.txnrights is None:
			return
		newr = self.txnrights
		self.txnrights = { }
		oldr = self.rights
		changes = [ ]
		for (sel,nrgt) in newr.items ():
			orgt = oldr.get (sel, '')
			grant = set (nrgt) - set (orgt)
			revok = set (orgt) - set (nrgt)
			if len (grant) != 0 or len (revok) != 0:
				grant = ''.join (grant)
				revok = ''.join (revok)
				changes.append ( (sel,grant,revok) )
		for (sel,orgt) in oldr.items ():
			if sel not in newr:
				grant = ''
				revok = orgt
				changes.append ( (sel,grant,revok) )
		self.rights = newr
		self.notify_changes (changes)
		assert self.txnrights == { }, 'There were ACL changes during txn_commit()'
		self.txnrights = None
		print ('DEBUG: The new ACL rights are %r' % self.rights)


# Lazy people (like me) enjoy short names
#
ACL = AccessControlList


def _parse_groupexp (groupid):
	"""Reduce a group expression to a tuple with:
	   
	    - A flag that indicates default-included
	    - A set of inverting members
	   
	   There is no overlap between the sets.  The
	   expressions are "intuitive", but that puts
	   a bit of strain on a parser :-S
	   
	   group+member0... -- False,member0,...
	   
	   group+-+member0... -- True,member0,...
	   
	   The first +-+ is decisive.  Later ones are
	   possible, but they only cut back on the
	   inverting members until they invert again.
	   
	   The groupexp must be supplied as a list,
	   excluding the base name.
	"""
	assert type (groupid) == list, 'The groupid should be an list of alias words'
	if groupid == []:
		default_included = True
	elif groupid [0] == '-':
		default_included = True
	else:
		default_included = False
	rejecting = False
	exceptions = set()
	for gid in groupid:
		if gid == '-':
			rejecting = not rejecting
		elif rejecting == default_included:
			# 1. default_included and rejected => exception
			# 2. suppressed   and not rejected => exception
			exceptions.add (gid.lower ())
	return (default_included,exceptions)


class Group (Identity):
	"""Groups are a form of Identity that represents the Identity of
	   many members of the group.  Syntactically they look like any
	   individual user, except that the form of aliases is an expression
	   that can address one or more list member, or all but some.
	   
	   Two kinds of service are likely to implement group behaviour:
	   
	    - Pull Services that passively await contact by members
	    - Push Services that actively seek contact with members
	   
	   In both cases, the question of membership arises, but in the
	   first it is only a test during an access attempt that may have
	   an identity translation to the domain as a side-effect; in the
	   second case a list of member addresses must be dished out.
	   
	   These two service kinds guided the API for this class.
	"""


	def __init__ (self, identity=None, domain=None, escaped=False, members=[]):
		"""Instantiate a Group as we would any other identity.  The
		   provided members are a list or generator, by default an
		   empty group will be created.  Each of the members should
		   be represented in an (member_alias,covert_address) tuple,
		   where the member_alias is the personal alias added to the
		   basename for the group, and the covert_address gives the
		   individual forwarding address for the member.
		"""
		super(Group,self).__init__ (identity=identity, domain=domain, escaped=escaped)
		# Group.__init__ (self, identity=identity, domain=domain, escaped=escaped)
		assert self.basename is not None, 'Groups must have a base name in their Identity'
		assert self.aliases  is     None, 'Groups must not have aliases in their Identity'
		self.m2c = { }
		self.c2m = { }
		for (alias,covert) in members:
			self.add_member (alias, covert)


	def add_member (self, member_alias, covert_address):
		"""Add a member to the list, using the presented member_alias
		   as a list-bound address for the underlying covert_address.
		   The list will make any effort to replace the covert_address
		   with the member_alias.
		   
		   The member_alias should start with "+" but it
		   may prefix the group basename; it does not
		   need an "@" but may have one if it is followed
		   by the group domain.
		   
		   TODO: Any use in multi-level member aliases?
		   TODO: Consider support for signed addresses.
		"""
		member_alias = member_alias.lower ()
		if not member_alias.startswith ('+'):
			assert member_alias.startswith (self.basename + '+'), 'Bad member_alias: different group basename'
			member_alias = member_alias [len (self.basename):]
		atidx = member_alias.find ('@')
		if atidx != -1:
			assert member_alias [atidx:] == '@' + domain, 'Bad member_alias: different group domain'
			member_alias = member_alias [:atidx]
		self.m2c [member_alias  ] = covert_address
		self.c2m [covert_address] = member_alias
		print ('DEBUG: add_member: Alias %s conceals %s' % (member_alias,covert_address))


	def del_member (self, member_alias):
		"""Remove the member_alias from the list.
		"""
		member_alias = member_alias.lower ()
		if member_alias not in self.m2c:
			return
		covert_address = self.m2c [member_alias]
		del self.m2c [member_alias  ]
		del self.c2m [covert_address]
		print ('DEBUG: del_member: Alias %s does no longer resolve to %s' % (member_alias,covert_address))


	def find_member_alias (self, covert_address):
		"""Test if the covert address occurs in the list,
		   and if so, expand it to a member Identity.
		   If the address is not found, return None.
		   
		   It may often be useful to add a signature
		   to channel responses, perhaps to a topic or
		   session, and maybe even to a particular remote
		   party.  This, however, is strongly dependent
		   on the application.  Note however that the
		   member name already conceals an underlying
		   address, so the use for encryption is minor.
		"""
		if covert_address not in self.c2m:
			return None
		membali = self.c2m [covert_address]
		membadr = '%s+%s@%s' @ (self.basename, membali, self.domain)
		return Identity (membadr)


	def list_members (self, groupexp=[]):
		"""List members, presumably for push-mode services.  The
		   groupexp provides aliases as appended to the group
		   name, and possibly showing detailed instructions,
		   
		   group+member0 -- to address just the individual member
		   
		   group+member0+member1 -- to address multiple members
		   
		   group+-+member0 -- to remove member0 from the list
		   
		   This function returns a generator for pairs of
		   (member_identity,covert_address) where the first
		   element is an arpa2.Identity address and the latter
		   a string.  The idea is that the member_identity is
		   used where possible and that the covert_address
		   only serves routing, ideally concealed from others,
		   such as is the case with SMTP envelope addresses.
		   
		   TODO: Avoid sending things more than once.
		   TODO: Consider mapping the members to arpa2.Identity
		"""
		(dflt,excp) = _parse_groupexp (groupexp)
		for (m,c) in self.m2c.items ():
			plus = m.find ('+', 1)
			if plus == -1:
				mb = m
			else:
				mb = m [:plus]
			if dflt != (mb in excp):
				# 1.     default and no exception to that
				# 2. not default but an exception to that
				yield (m,c)


# TEST CODE

if __name__ == "__main__":
	import uuid
	class MyACL (ACL):
		def notify_changes (self, notes):
			print ('Transactional update:', notes)
	acl = MyACL ( [ '100 %RWCV john@example.com mary+@example.com %R @example.com %V @.' ], 'X' )
	acl.txn_begin ()
	acl.txn_grant ('bakker@orvelte.nep', 'C')
	acl.txn_grant ('smid@orvelte.nep', 'RWC')
	acl.txn_commit ()
	acl.txn_begin ()
	acl.txn_grant ('bakker@orvelte.nep', 'RW')
	acl.txn_revoke ('smid@orvelte.nep', 'RWC')
	acl.txn_commit ()
	context = {
		'sessionId': str (uuid.uuid4 ()),
		'humanSubject': 'De kat krabt de krullen van de trap',
		'automationTopic': '[mew]',
		'encryptedLocalPart': True,
		'expirationDays': 32,
		'remoteIdentity': Identity ('bakker@orvelte.nep'),
		'remoteUseDomain': True,
		'remoteUseLocalPart': True,
		'remoteUseAliases': True,
	}
	def test (id0):
		id = Identity (id0)
		print ('Original %s' % id)
		print ('id.basename', id.basename, '.aliases', id.aliases, '.domain', id.domain)
		for g0 in id.selectors ():
			print (' --> %s' % g0)
		print ('ACL rights', acl.check_rights (id))
		id.update_signature (**context)
		print ('Signed   %s' % id)
		id.check_signature (**context)
		print ('Verified %s' % id)
		print ('Compares %s' % id0)
		print ()
	test ('john@example.com')
	test (u'jøhn@example.com')
	#SelectorOnly# test ('john+@example.com')
	test ('mary+home@example.com')
	test ('mary+home+alone@example.com')
	test (u'mæry+høme+aloñe@example.com')
	test ('+eliza@example.com')
	test ('+eliza+today@example.com')
	test ('+eliza+today+tomorrow@example.com')
	test ('john+signore+@example.com')
	test ('mary+signorina+@example.com')
	test ('mary+home+alone+signorina+@example.com')
	test ('+eliza+signaling+@example.com')
	test ('+eliza+today+signaling+@example.com')
	test ('+eliza+today+tomorrow+signaling+@example.com')
	#
	# Create a group and add members to it
	# grp = Group ('ambacht@orvelte.nep',None,None)
	# grp = Group ('ambacht@orvelte.nep',None,None,[ ('+bakker','john@example.com'), ('+smid','mary@example.com') ])
	grp = Group (identity='ambacht@orvelte.nep', members=[ ('+bakker','john@example.com'), ('+smid','mary@example.com') ] )
	print ('Members of group:\n%r' % [g for g in grp.list_members ()])
	grp.add_member ('+kuiper','eliza@balloo.com')
	print ('Members of group:\n%r' % [g for g in grp.list_members ()])
	print ('Members of group (except smid):\n%r' % [g for g in grp.list_members (['-','+smid'])])
	print ('Members of group (only bakker and kuiper):\n%r' % [g for g in grp.list_members (['+bakker','+kuiper'])])
	grp.del_member ('+smid')
	print ('Members of group (after removal of smid):\n%r' % [g for g in grp.list_members ()])

