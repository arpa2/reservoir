#!/usr/bin/env python3
#
# MusicLister.py / .txt -- List music and play it
#
# This is a minimal Reservoir application, based on the API, and it
# has served as a learning experience.  Having said that, it is also
# a good demonstration of the interaction between a web-frontend and
# a Reservoir backend.
#
# This was developed together with the Docker image arpa2/files-reservoir
# containing an LDIF and a matching /var/arpa2/reservoir tree that was
# manually constructed as bootstrapping data.  This image distributes
# a wonderful album "Whispering" by "Negrin", who kindly made it available
# under a Creative Commons License.  Thank you, Negrin :) the music is an
# interesting surprise to run into after setting this up.
#
# By Henri Manson and Rick van Rein


import sys
import os.path
from urllib.parse import quote, unquote

from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.template import Element, renderer, XMLFile, flattenString
from twisted.python.filepath import FilePath

from arpa2 import reservoir


class WidgetsElement(Element):
    filename = os.path.dirname(sys.argv[0]) + '/' + 'MusicLister.xml'
    loader = XMLFile(FilePath(filename))

    @renderer
    def widgets(self, request, tag):
        for widget in self.widgetData:
            yield tag.clone().fillSlots(href=widget['href'], content=widget['content'])


arg2ldap = [ 'cn', 'mediaType', 'uniqueIdentifier', 'documentTitle', 'documentHash' ]

host2domain = {
    'localhost:8811': 'arpa2.org',
}


class MusicList(Resource):
    isLeaf = True

    def printResult(self, result):
        self.request.write(result)
        self.request.finish()

    def render_GET(self, request):
        try:
            #DEBUG# print ('Trying to open as domain-relative URI:\n%r' % request.path.decode('utf-8'))
            host = request.getHeader('host')
            domain = host2domain [host]
            (clx,resource) = reservoir.uri_canonical_open(request.path.decode('utf-8'), domain_relative=True, domain=domain)

            if resource is None:
                w = WidgetsElement()
                w.widgetData = [ ]
                for (name,uuid) in clx.items():
                    entry = { 'href': '/%s/' % uuid, 'content': name }
                    w.widgetData.append(entry)
                flt = '(&'
                #DEBUG# for k in request.args.keys ():
                #DEBUG#     print ('Argument %s is %r' % (k,request.args[k]))
                for a2l in arg2ldap:
                    a2l_b = bytes(a2l, 'utf-8')
                    if a2l_b in request.args:
                        flt += '(|'
                        for ra in request.args[a2l_b]:
                            ra = quote (ra).replace ('%2A', '*')
                            flt += '(%s=%s)' % (a2l,ra)
                        flt += ')'
                flt += ')'
                #DEBUG# print ('Search filter: %r' % flt)
                try:
                    resources = reservoir.search_resources (clx, flt).values ()
                    #DEBUG# print ('Resources from search: %r' % resources)
                except:
                    resources = clx.load_all_resources()
                    #DEBUG# print ('Resources from load: %r' % resources)
                for resource in resources:
                    result = ""
                    #DEBUG# print ('Resource: %r' % resource)
                    for cn in resource['cn']:
                        result += cn
                    #DEBUG# print ('Result: %r' % result)
                    entry = { 'href': resource.resuuid, 'content': result }
                    w.widgetData.append(entry)
                d = flattenString(None, w)
                self.request = request
                d.addCallback(self.printResult)
                return NOT_DONE_YET
            else:
                fileContent = resource.open(binary=True).read()
                for media_type in resource['mediaType']:
                    request.setHeader('Content-type', media_type)
                for description in resource.get ('description',[]):
                    request.setHeader('Content-description', description)
                for filename in resource.get ('uniqueIdentifier',[]):
                    request.setHeader('Content-disposition', 'inline;filename="%s"' % filename)
                return fileContent
        except Exception as exc:
            print (exc)
            user = request.getHeader('user')
            if (user is not None):
                user = unquote(user)
            (uri,clx,resource) = reservoir.uri_canonical(domain=domain, apphint='Music', domain_relative=True, user=user)
            args = request.uri[len(request.path):]
            if args[:1] != b'?':
                args = b''
            #DEBUG# print ('Redirecting to %s' % uri)
            request.redirect(uri.encode('utf-8') + args)
            request.finish()
            return NOT_DONE_YET


factory = Site(MusicList())
reactor.listenTCP(8811, factory)
reactor.run()
